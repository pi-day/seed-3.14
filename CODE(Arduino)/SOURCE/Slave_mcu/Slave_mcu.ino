//===================LIBRARIES=================
#include "DHT.h"

#include <SPI.h>
#include <SD.h>

#include <Wire.h>

#include <BMx280MI.h>

#include <MPU9255.h>//include MPU9255 library



#include <stdio.h>

//===================END LIBRARIES=============

//===================VARIABLES=================

//DHT11
#define DHTPIN 2   
#define DHTTYPE DHT11 
DHT dht(DHTPIN, DHTTYPE);

//SD card
File myFile;


//MPU
MPU9255 mpu;

#define I2C_ADDRESS 0x76
//create a BMx280I2C object using the I2C interface with I2C Address 0x76
BMx280I2C bmx280(I2C_ADDRESS);


//=================END VARIABLES===============
float Res[9]; //results from sensors 

//=======================FOR_DHT11=============================

void setup() {
 Serial.begin(9600);
  //wait for serial connection to open (only necessary on some boards)
  while (!Serial);
 
 //Servo
 //Ser_1.attach(9);

//DHT11
 dht.begin();

  if (!SD.begin(10)) {
    Serial.println("SD initialization failed!");
    while (1);
  }

 //==============MPU9255 CODE BEGIN (VOID SETUP)================
 
 if(mpu.init())
  {
  Serial.println("MPU initialization failed");
  }
//  else
//  {
//  Serial.println("initialization succesful!");
//  }
 //=============MPU9255 CODE END (VOID SETUP)===================

  Wire.begin();
 // register event
  //begin() checks the Interface, reads the sensor ID (to differentiate between BMP280 and BME280)
  //and reads compensation parameters.
  if (!bmx280.begin())
  {
    Serial.println("begin() failed. check your BMx280 Interface and I2C Address.");
    while (1);
  }

//  if (bmx280.isBME280())
//    Serial.println("sensor is a BME280");
//  else
//    Serial.println("sensor is a BMP280");

  //reset sensor to default parameters.
  bmx280.resetToDefaults();

  //by default sensing is disabled and must be enabled by setting a non-zero
  //oversampling setting.
  //set an oversampling setting for pressure and temperature measurements. 
  bmx280.writeOversamplingPressure(BMx280MI::OSRS_P_x16);
  bmx280.writeOversamplingTemperature(BMx280MI::OSRS_T_x16);

  //if sensor is a BME280, set an oversampling setting for humidity measurements.
//  if (bmx280.isBME280())
//    bmx280.writeOversamplingHumidity(BMx280MI::OSRS_H_x16);

}//End of Setup


void loop() {

  delay(2000);
//===============================SERVO==========================
  //Ser_1.write(90);
//==============================DHT11 READINGS START================

  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  Res[0] = dht.readHumidity();
  
  // Read temperature as Celsius (the default)
  Res[1] = dht.readTemperature();


//==============================DHT11 READINGS END================

//====================HW_612(LOOP)====================


  //start a measurement
  if (!bmx280.measure())
  {
    Serial.println("could not start measurement, is a measurement already running?");
    return;
  }

  //wait for the measurement to finish
  do
  {
    delay(100);
  } while (!bmx280.hasValue());

   Res[2] = bmx280.getPressure();
   


   
//======================MPU9255 CODE BEGIN (LOOP)=======================
  mpu.read_acc();//get data from the accelerometer
  mpu.read_gyro();//get data from the gyroscope
  //mpu.read_mag();//get data from the magnetometer
 
   Res[3] = mpu.ax;
   Res[4] = mpu.ay;  
   Res[5] = mpu.az;
   Res[6] = mpu.gx;   
   Res[7] = mpu.gy;   
   Res[8] = mpu.gz;
  // Res[9] = mpu.mx;
  // Res[10] = mpu.my;
  // Res[11] = mpu.mz;
  //print all data in serial monitor

  
  delay(100);
  
//=================MPU9255 CODE END (LOOP)=================


//  Serial.print("humidity is ");
//  Serial.println(Res[0]);
//  Serial.print("temperature is ");
//  Serial.println(Res[1]);
//  Serial.print("Pressure: "); 
//  Serial.println(Res[2]);
//
//  Serial.print("AX: ");
//  Serial.print(Res[3]);
//  
//  Serial.print(" AY: ");
//  Serial.print(Res[4]);
//  
//  Serial.print(" AZ: ");
//  Serial.print(Res[5]);
//  
//  Serial.print(" GX: ");
//  Serial.print(Res[6]);
//  
//  Serial.print(" GY: ");
//  Serial.print(Res[7]);
//  
//  Serial.print(" GZ: ");
//  Serial.print(Res[8]);
//  
//  Serial.print(" MX: ");
//  Serial.print(Res[9]);
//  
//  Serial.print(" MY: ");
//  Serial.print(Res[10]);
//  
//  Serial.print(" MZ: ");
//  Serial.println(Res[11]);

//==============================CONVERT TO CHAR[] (SD-DHT11)======

myFile = SD.open("test.txt", FILE_WRITE);
      
if (myFile) {

    Serial.print("Writing to test.txt...");

    for (int i = 0; i <= 8; i ++){
      myFile.println(Res[i]); 
      delay(90);     
    }

    myFile.println("=");
    // close the file:
    myFile.close();
    Serial.println("done.");
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening test.txt");
  }

}//End of loop


 
