<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.5.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="16" fill="1" visible="no" active="no"/>
<layer number="3" name="Route3" color="17" fill="1" visible="no" active="no"/>
<layer number="4" name="Route4" color="18" fill="1" visible="no" active="no"/>
<layer number="5" name="Route5" color="19" fill="1" visible="no" active="no"/>
<layer number="6" name="Route6" color="25" fill="1" visible="no" active="no"/>
<layer number="7" name="Route7" color="26" fill="1" visible="no" active="no"/>
<layer number="8" name="Route8" color="27" fill="1" visible="no" active="no"/>
<layer number="9" name="Route9" color="28" fill="1" visible="no" active="no"/>
<layer number="10" name="Route10" color="29" fill="1" visible="no" active="no"/>
<layer number="11" name="Route11" color="30" fill="1" visible="no" active="no"/>
<layer number="12" name="Route12" color="20" fill="1" visible="no" active="no"/>
<layer number="13" name="Route13" color="21" fill="1" visible="no" active="no"/>
<layer number="14" name="Route14" color="22" fill="1" visible="no" active="no"/>
<layer number="15" name="Route15" color="23" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="yes" active="yes"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="no" active="no"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="no" active="no"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="VCC" urn="urn:adsk.eagle:symbol:26928/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VCC" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+3V3" urn="urn:adsk.eagle:symbol:26950/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC" urn="urn:adsk.eagle:component:26957/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VCC" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3V3" urn="urn:adsk.eagle:component:26981/1" prefix="+3V3" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="2x4">
<packages>
<package name="2X3">
<pad name="P$1" x="-2.54" y="3.81" drill="0.8" diameter="1.4224"/>
<pad name="P$2" x="-2.54" y="1.27" drill="0.8" diameter="1.4224"/>
<pad name="P$3" x="-2.54" y="-1.27" drill="0.8" diameter="1.4224"/>
<pad name="P$6" x="2.54" y="-1.27" drill="0.8" diameter="1.4224"/>
<pad name="P$7" x="2.54" y="1.27" drill="0.8" diameter="1.4224"/>
<pad name="P$8" x="2.54" y="3.81" drill="0.8" diameter="1.4224"/>
<text x="0" y="5.08" size="1.27" layer="25">&gt;NAME</text>
<text x="0" y="6.62" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-4" y1="5" x2="4" y2="5" width="0.127" layer="21"/>
<wire x1="4" y1="5" x2="4" y2="-2.46" width="0.127" layer="21"/>
<wire x1="4" y1="-2.46" x2="-4" y2="-2.46" width="0.127" layer="21"/>
<wire x1="-4" y1="-2.46" x2="-4" y2="5" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="2X3">
<pin name="P$1" x="-12.7" y="5.08" length="middle"/>
<pin name="P$2" x="-12.7" y="0" length="middle"/>
<pin name="P$3" x="-12.7" y="-5.08" length="middle"/>
<pin name="P$6" x="12.7" y="-5.08" length="middle" rot="R180"/>
<pin name="P$7" x="12.7" y="0" length="middle" rot="R180"/>
<pin name="P$8" x="12.7" y="5.08" length="middle" rot="R180"/>
<wire x1="-7.62" y1="7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<text x="-1.524" y="9.398" size="1.27" layer="95" rot="MR180">&gt;NAME</text>
<text x="-1.524" y="10.414" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="2X3">
<gates>
<gate name="G$1" symbol="2X3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2X3">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$6" pad="P$6"/>
<connect gate="G$1" pin="P$7" pad="P$7"/>
<connect gate="G$1" pin="P$8" pad="P$8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ATMEGA328P-AU">
<description>&lt;MICROCHIP - ATMEGA328P-AU. - MICROCONTROLLER MCU, 8 BIT, ATMEGA, 20MHZ, TQFP-32&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="QFP80P900X900X120-32N">
<description>&lt;b&gt;32A&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-4.25" y="2.8" dx="1.5" dy="0.6" layer="1"/>
<smd name="2" x="-4.25" y="2" dx="1.5" dy="0.6" layer="1"/>
<smd name="3" x="-4.25" y="1.2" dx="1.5" dy="0.6" layer="1"/>
<smd name="4" x="-4.25" y="0.4" dx="1.5" dy="0.6" layer="1"/>
<smd name="5" x="-4.25" y="-0.4" dx="1.5" dy="0.6" layer="1"/>
<smd name="6" x="-4.25" y="-1.2" dx="1.5" dy="0.6" layer="1"/>
<smd name="7" x="-4.25" y="-2" dx="1.5" dy="0.6" layer="1"/>
<smd name="8" x="-4.25" y="-2.8" dx="1.5" dy="0.6" layer="1"/>
<smd name="9" x="-2.8" y="-4.25" dx="1.5" dy="0.6" layer="1" rot="R90"/>
<smd name="10" x="-2" y="-4.25" dx="1.5" dy="0.6" layer="1" rot="R90"/>
<smd name="11" x="-1.2" y="-4.25" dx="1.5" dy="0.6" layer="1" rot="R90"/>
<smd name="12" x="-0.4" y="-4.25" dx="1.5" dy="0.6" layer="1" rot="R90"/>
<smd name="13" x="0.4" y="-4.25" dx="1.5" dy="0.6" layer="1" rot="R90"/>
<smd name="14" x="1.2" y="-4.25" dx="1.5" dy="0.6" layer="1" rot="R90"/>
<smd name="15" x="2" y="-4.25" dx="1.5" dy="0.6" layer="1" rot="R90"/>
<smd name="16" x="2.8" y="-4.25" dx="1.5" dy="0.6" layer="1" rot="R90"/>
<smd name="17" x="4.25" y="-2.8" dx="1.5" dy="0.6" layer="1"/>
<smd name="18" x="4.25" y="-2" dx="1.5" dy="0.6" layer="1"/>
<smd name="19" x="4.25" y="-1.2" dx="1.5" dy="0.6" layer="1"/>
<smd name="20" x="4.25" y="-0.4" dx="1.5" dy="0.6" layer="1"/>
<smd name="21" x="4.25" y="0.4" dx="1.5" dy="0.6" layer="1"/>
<smd name="22" x="4.25" y="1.2" dx="1.5" dy="0.6" layer="1"/>
<smd name="23" x="4.25" y="2" dx="1.5" dy="0.6" layer="1"/>
<smd name="24" x="4.25" y="2.8" dx="1.5" dy="0.6" layer="1"/>
<smd name="25" x="2.8" y="4.25" dx="1.5" dy="0.6" layer="1" rot="R90"/>
<smd name="26" x="2" y="4.25" dx="1.5" dy="0.6" layer="1" rot="R90"/>
<smd name="27" x="1.2" y="4.25" dx="1.5" dy="0.6" layer="1" rot="R90"/>
<smd name="28" x="0.4" y="4.25" dx="1.5" dy="0.6" layer="1" rot="R90"/>
<smd name="29" x="-0.4" y="4.25" dx="1.5" dy="0.6" layer="1" rot="R90"/>
<smd name="30" x="-1.2" y="4.25" dx="1.5" dy="0.6" layer="1" rot="R90"/>
<smd name="31" x="-2" y="4.25" dx="1.5" dy="0.6" layer="1" rot="R90"/>
<smd name="32" x="-2.8" y="4.25" dx="1.5" dy="0.6" layer="1" rot="R90"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-5.25" y1="5.25" x2="5.25" y2="5.25" width="0.05" layer="51"/>
<wire x1="5.25" y1="5.25" x2="5.25" y2="-5.25" width="0.05" layer="51"/>
<wire x1="5.25" y1="-5.25" x2="-5.25" y2="-5.25" width="0.05" layer="51"/>
<wire x1="-5.25" y1="-5.25" x2="-5.25" y2="5.25" width="0.05" layer="51"/>
<wire x1="-3.5" y1="3.5" x2="3.5" y2="3.5" width="0.1" layer="51"/>
<wire x1="3.5" y1="3.5" x2="3.5" y2="-3.5" width="0.1" layer="51"/>
<wire x1="3.5" y1="-3.5" x2="-3.5" y2="-3.5" width="0.1" layer="51"/>
<wire x1="-3.5" y1="-3.5" x2="-3.5" y2="3.5" width="0.1" layer="51"/>
<wire x1="-3.5" y1="2.7" x2="-2.7" y2="3.5" width="0.1" layer="51"/>
<wire x1="-3.15" y1="3.15" x2="3.15" y2="3.15" width="0.2" layer="21"/>
<wire x1="3.15" y1="3.15" x2="3.15" y2="-3.15" width="0.2" layer="21"/>
<wire x1="3.15" y1="-3.15" x2="-3.15" y2="-3.15" width="0.2" layer="21"/>
<wire x1="-3.15" y1="-3.15" x2="-3.15" y2="3.15" width="0.2" layer="21"/>
<circle x="-4.6" y="4" radius="0.2" width="0.4" layer="25"/>
</package>
</packages>
<symbols>
<symbol name="ATMEGA328P-AU">
<wire x1="5.08" y1="30.48" x2="45.72" y2="30.48" width="0.254" layer="94"/>
<wire x1="45.72" y1="-40.64" x2="45.72" y2="30.48" width="0.254" layer="94"/>
<wire x1="45.72" y1="-40.64" x2="5.08" y2="-40.64" width="0.254" layer="94"/>
<wire x1="5.08" y1="30.48" x2="5.08" y2="-40.64" width="0.254" layer="94"/>
<text x="46.99" y="35.56" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="46.99" y="33.02" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="PD3_[PCINT19]" x="0" y="0" length="middle"/>
<pin name="PD4_[PCINT20]" x="0" y="-2.54" length="middle"/>
<pin name="GND_1" x="0" y="-5.08" length="middle" direction="pwr"/>
<pin name="VCC_1" x="0" y="-7.62" length="middle" direction="pwr"/>
<pin name="GND_2" x="0" y="-10.16" length="middle" direction="pwr"/>
<pin name="VCC_2" x="0" y="-12.7" length="middle" direction="pwr"/>
<pin name="PB6_[PCINT6]" x="0" y="-15.24" length="middle"/>
<pin name="PB7_[PCINT7]" x="0" y="-17.78" length="middle"/>
<pin name="PD5_[PCINT21]" x="15.24" y="-45.72" length="middle" rot="R90"/>
<pin name="PD6_[PCINT22]" x="17.78" y="-45.72" length="middle" rot="R90"/>
<pin name="PD7_[PCINT23]" x="20.32" y="-45.72" length="middle" rot="R90"/>
<pin name="PB0_[PCINT0]" x="22.86" y="-45.72" length="middle" rot="R90"/>
<pin name="PB1_[PCINT1]" x="25.4" y="-45.72" length="middle" rot="R90"/>
<pin name="PB2_[PCINT2]" x="27.94" y="-45.72" length="middle" rot="R90"/>
<pin name="PB3_[PCINT3]" x="30.48" y="-45.72" length="middle" rot="R90"/>
<pin name="PB4_[PCINT4]" x="33.02" y="-45.72" length="middle" rot="R90"/>
<pin name="PC1_[PCINT9]" x="50.8" y="0" length="middle" rot="R180"/>
<pin name="PC0_[PCINT8]" x="50.8" y="-2.54" length="middle" rot="R180"/>
<pin name="ADC7" x="50.8" y="-5.08" length="middle" rot="R180"/>
<pin name="GND_3" x="50.8" y="-7.62" length="middle" direction="pwr" rot="R180"/>
<pin name="AREF" x="50.8" y="-10.16" length="middle" rot="R180"/>
<pin name="ADC6" x="50.8" y="-12.7" length="middle" rot="R180"/>
<pin name="AVCC" x="50.8" y="-15.24" length="middle" direction="pwr" rot="R180"/>
<pin name="PB5_[PCINT5]" x="50.8" y="-17.78" length="middle" rot="R180"/>
<pin name="PD2_[PCINT18]" x="15.24" y="35.56" length="middle" rot="R270"/>
<pin name="PD1_[PCINT17]" x="17.78" y="35.56" length="middle" rot="R270"/>
<pin name="PD0_[PCINT16]" x="20.32" y="35.56" length="middle" rot="R270"/>
<pin name="PC6_[RESET/PCINT14]" x="22.86" y="35.56" length="middle" rot="R270"/>
<pin name="PC5_[PCINT13]" x="25.4" y="35.56" length="middle" rot="R270"/>
<pin name="PC4_[PCINT12]" x="27.94" y="35.56" length="middle" rot="R270"/>
<pin name="PC3_[PCINT11]" x="30.48" y="35.56" length="middle" rot="R270"/>
<pin name="PC2_[PCINT10]" x="33.02" y="35.56" length="middle" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ATMEGA328P-AU" prefix="IC">
<description>&lt;b&gt;MICROCHIP - ATMEGA328P-AU. - MICROCONTROLLER MCU, 8 BIT, ATMEGA, 20MHZ, TQFP-32&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://componentsearchengine.com/Datasheets/2/ATMEGA328P-AU.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="ATMEGA328P-AU" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFP80P900X900X120-32N">
<connects>
<connect gate="G$1" pin="ADC6" pad="19"/>
<connect gate="G$1" pin="ADC7" pad="22"/>
<connect gate="G$1" pin="AREF" pad="20"/>
<connect gate="G$1" pin="AVCC" pad="18"/>
<connect gate="G$1" pin="GND_1" pad="3"/>
<connect gate="G$1" pin="GND_2" pad="5"/>
<connect gate="G$1" pin="GND_3" pad="21"/>
<connect gate="G$1" pin="PB0_[PCINT0]" pad="12"/>
<connect gate="G$1" pin="PB1_[PCINT1]" pad="13"/>
<connect gate="G$1" pin="PB2_[PCINT2]" pad="14"/>
<connect gate="G$1" pin="PB3_[PCINT3]" pad="15"/>
<connect gate="G$1" pin="PB4_[PCINT4]" pad="16"/>
<connect gate="G$1" pin="PB5_[PCINT5]" pad="17"/>
<connect gate="G$1" pin="PB6_[PCINT6]" pad="7"/>
<connect gate="G$1" pin="PB7_[PCINT7]" pad="8"/>
<connect gate="G$1" pin="PC0_[PCINT8]" pad="23"/>
<connect gate="G$1" pin="PC1_[PCINT9]" pad="24"/>
<connect gate="G$1" pin="PC2_[PCINT10]" pad="25"/>
<connect gate="G$1" pin="PC3_[PCINT11]" pad="26"/>
<connect gate="G$1" pin="PC4_[PCINT12]" pad="27"/>
<connect gate="G$1" pin="PC5_[PCINT13]" pad="28"/>
<connect gate="G$1" pin="PC6_[RESET/PCINT14]" pad="29"/>
<connect gate="G$1" pin="PD0_[PCINT16]" pad="30"/>
<connect gate="G$1" pin="PD1_[PCINT17]" pad="31"/>
<connect gate="G$1" pin="PD2_[PCINT18]" pad="32"/>
<connect gate="G$1" pin="PD3_[PCINT19]" pad="1"/>
<connect gate="G$1" pin="PD4_[PCINT20]" pad="2"/>
<connect gate="G$1" pin="PD5_[PCINT21]" pad="9"/>
<connect gate="G$1" pin="PD6_[PCINT22]" pad="10"/>
<connect gate="G$1" pin="PD7_[PCINT23]" pad="11"/>
<connect gate="G$1" pin="VCC_1" pad="4"/>
<connect gate="G$1" pin="VCC_2" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="MICROCHIP - ATMEGA328P-AU. - MICROCONTROLLER MCU, 8 BIT, ATMEGA, 20MHZ, TQFP-32" constant="no"/>
<attribute name="HEIGHT" value="1.2mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Microchip" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="ATMEGA328P-AU" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="556-ATMEGA328P-AU" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.com/Search/Refine.aspx?Keyword=556-ATMEGA328P-AU" constant="no"/>
<attribute name="RS_PART_NUMBER" value="1310271P" constant="no"/>
<attribute name="RS_PRICE-STOCK" value="http://uk.rs-online.com/web/p/products/1310271P" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="R1206">
<description>&lt;Thick Film Resistors - SMD 2.49K OHM 1%&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="RESC3116X65N">
<description>&lt;b&gt;RC1206&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-1.5" y="0" dx="1.75" dy="1" layer="1" rot="R90"/>
<smd name="2" x="1.5" y="0" dx="1.75" dy="1" layer="1" rot="R90"/>
<text x="0" y="-1.905" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="2.032" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-2.25" y1="1.15" x2="2.25" y2="1.15" width="0.05" layer="21"/>
<wire x1="2.25" y1="1.15" x2="2.25" y2="-1.15" width="0.05" layer="21"/>
<wire x1="2.25" y1="-1.15" x2="-2.25" y2="-1.15" width="0.05" layer="21"/>
<wire x1="-2.25" y1="-1.15" x2="-2.25" y2="1.15" width="0.05" layer="21"/>
<wire x1="-1.55" y1="0.8" x2="1.55" y2="0.8" width="0.1" layer="51"/>
<wire x1="1.55" y1="0.8" x2="1.55" y2="-0.8" width="0.1" layer="51"/>
<wire x1="1.55" y1="-0.8" x2="-1.55" y2="-0.8" width="0.1" layer="51"/>
<wire x1="-1.55" y1="-0.8" x2="-1.55" y2="0.8" width="0.1" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="RC1206FR-072K49L">
<wire x1="-5.08" y1="1.27" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="-5.08" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="-1.27" width="0.254" layer="94"/>
<text x="3.81" y="6.35" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="3.81" y="3.81" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="1" x="-10.16" y="0" visible="pad" length="middle"/>
<pin name="2" x="7.62" y="0" visible="pad" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RC1206FR-072K49L" prefix="R">
<description>&lt;b&gt;Thick Film Resistors - SMD 2.49K OHM 1%&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://www.sos.sk/productdata/35/10/4/35104/RC1206.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="RC1206FR-072K49L" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RESC3116X65N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Thick Film Resistors - SMD 2.49K OHM 1%" constant="no"/>
<attribute name="HEIGHT" value="0.65mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="YAGEO (PHYCOMP)" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="RC1206FR-072K49L" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="603-RC1206FR-072K49L" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.com/Search/Refine.aspx?Keyword=603-RC1206FR-072K49L" constant="no"/>
<attribute name="RS_PART_NUMBER" value="" constant="no"/>
<attribute name="RS_PRICE-STOCK" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="C1206">
<description>&lt;Multilayer Ceramic Capacitors MLCC - SMD/SMT 50V 10uF X5R 1206 10% HIGH CV&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="CAPC3216X229N">
<description>&lt;b&gt;12065D106KAT2A-1&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-1.4" y="0" dx="1.82" dy="1.02" layer="1" rot="R90"/>
<smd name="2" x="1.4" y="0" dx="1.82" dy="1.02" layer="1" rot="R90"/>
<text x="0" y="-2.286" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="-0.127" y="1.778" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-2.06" y1="1.06" x2="2.06" y2="1.06" width="0.05" layer="21"/>
<wire x1="2.06" y1="1.06" x2="2.06" y2="-1.06" width="0.05" layer="21"/>
<wire x1="2.06" y1="-1.06" x2="-2.06" y2="-1.06" width="0.05" layer="21"/>
<wire x1="-2.06" y1="-1.06" x2="-2.06" y2="1.06" width="0.05" layer="21"/>
<wire x1="-1.6" y1="0.8" x2="1.6" y2="0.8" width="0.1" layer="51"/>
<wire x1="1.6" y1="0.8" x2="1.6" y2="-0.8" width="0.1" layer="51"/>
<wire x1="1.6" y1="-0.8" x2="-1.6" y2="-0.8" width="0.1" layer="51"/>
<wire x1="-1.6" y1="-0.8" x2="-1.6" y2="0.8" width="0.1" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="12065D106KAT2A">
<wire x1="0.508" y1="2.54" x2="0.508" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.032" y1="2.54" x2="2.032" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0.508" y2="0" width="0.254" layer="94"/>
<wire x1="2.032" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<text x="3.81" y="6.35" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="3.81" y="3.81" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="pad" length="middle"/>
<pin name="2" x="7.62" y="0" visible="pad" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="12065D106KAT2A" prefix="C">
<description>&lt;b&gt;Multilayer Ceramic Capacitors MLCC - SMD/SMT 50V 10uF X5R 1206 10% HIGH CV&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://datasheets.avx.com/cx5r.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="12065D106KAT2A" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CAPC3216X229N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Multilayer Ceramic Capacitors MLCC - SMD/SMT 50V 10uF X5R 1206 10% HIGH CV" constant="no"/>
<attribute name="HEIGHT" value="2.29mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="AVX" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="12065D106KAT2A" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="581-12065D106KAT2A" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.com/Search/Refine.aspx?Keyword=581-12065D106KAT2A" constant="no"/>
<attribute name="RS_PART_NUMBER" value="" constant="no"/>
<attribute name="RS_PRICE-STOCK" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="LORA_MODULE">
<packages>
<package name="LORA_MODULE">
<pad name="GND" x="-12.7" y="7.62" drill="0.8" diameter="1.4224"/>
<pad name="DIO4" x="-12.7" y="5.08" drill="0.8" diameter="1.4224"/>
<pad name="DIO5" x="-12.7" y="2.54" drill="0.8" diameter="1.4224"/>
<pad name="SCK" x="-12.7" y="0" drill="0.8" diameter="1.4224"/>
<pad name="MISO" x="-12.7" y="-2.54" drill="0.8" diameter="1.4224"/>
<pad name="MOSI" x="-12.7" y="-5.08" drill="0.8" diameter="1.4224"/>
<pad name="NSS" x="-12.7" y="-7.62" drill="0.8" diameter="1.4224"/>
<pad name="GND@1" x="-12.7" y="-10.16" drill="0.8" diameter="1.4224"/>
<pad name="DIO3" x="12.7" y="7.54" drill="0.8" diameter="1.4224"/>
<pad name="DIO2" x="12.7" y="5" drill="0.8" diameter="1.4224"/>
<pad name="DIO1" x="12.7" y="2.46" drill="0.8" diameter="1.4224"/>
<pad name="DIO0" x="12.7" y="-0.08" drill="0.8" diameter="1.4224"/>
<pad name="RST" x="12.7" y="-2.62" drill="0.8" diameter="1.4224"/>
<pad name="3.3V" x="12.7" y="-5.16" drill="0.8" diameter="1.4224"/>
<pad name="GND@3" x="12.7" y="-7.7" drill="0.8" diameter="1.4224"/>
<pad name="GND@2" x="12.7" y="-10.24" drill="0.8" diameter="1.4224"/>
<text x="0" y="7.62" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.08" y="5.08" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-10.16" y1="-10.16" x2="7.62" y2="-10.16" width="0.127" layer="21"/>
<wire x1="7.62" y1="-10.16" x2="10.16" y2="-10.16" width="0.127" layer="21"/>
<wire x1="7.62" y1="-10.16" x2="7.62" y2="-7.62" width="0.127" layer="21"/>
<wire x1="10.16" y1="-7.62" x2="7.62" y2="-7.62" width="0.127" layer="21"/>
<wire x1="-10.16" y1="7.62" x2="-2.54" y2="7.62" width="0.127" layer="21"/>
<dimension x1="-13.589" y1="7.62" x2="13.589" y2="7.62" x3="0" y3="10.16" textsize="1.27" layer="51"/>
<dimension x1="-12.7" y1="8.509" x2="-12.7" y2="-11.049" x3="-13.97" y3="-1.27" textsize="1.27" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="LORA_MODULE">
<pin name="GND" x="-15.24" y="20.32" length="middle"/>
<pin name="DIO4" x="-15.24" y="15.24" length="middle"/>
<pin name="DIO5" x="-15.24" y="10.16" length="middle"/>
<pin name="SCK" x="-15.24" y="5.08" length="middle"/>
<pin name="MISO" x="-15.24" y="0" length="middle"/>
<pin name="MOSI" x="-15.24" y="-5.08" length="middle"/>
<pin name="NSS" x="-15.24" y="-10.16" length="middle"/>
<pin name="GND@1" x="-15.24" y="-15.24" length="middle"/>
<pin name="GND@2" x="17.78" y="-15.24" length="middle" rot="R180"/>
<pin name="GND@3" x="17.78" y="-10.16" length="middle" rot="R180"/>
<pin name="3.3V" x="17.78" y="-5.08" length="middle" rot="R180"/>
<pin name="RST" x="17.78" y="0" length="middle" rot="R180"/>
<pin name="DIO0" x="17.78" y="5.08" length="middle" rot="R180"/>
<pin name="DIO1" x="17.78" y="10.16" length="middle" rot="R180"/>
<pin name="DIO2" x="17.78" y="15.24" length="middle" rot="R180"/>
<pin name="DIO3" x="17.78" y="20.32" length="middle" rot="R180"/>
<wire x1="-10.16" y1="22.86" x2="-10.16" y2="-17.78" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-17.78" x2="12.7" y2="-17.78" width="0.254" layer="94"/>
<wire x1="12.7" y1="-17.78" x2="12.7" y2="22.86" width="0.254" layer="94"/>
<wire x1="12.7" y1="22.86" x2="-10.16" y2="22.86" width="0.254" layer="94"/>
<text x="-2.54" y="25.4" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="22.86" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="LORA_MODULE">
<gates>
<gate name="G$1" symbol="LORA_MODULE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LORA_MODULE">
<connects>
<connect gate="G$1" pin="3.3V" pad="3.3V"/>
<connect gate="G$1" pin="DIO0" pad="DIO0"/>
<connect gate="G$1" pin="DIO1" pad="DIO1"/>
<connect gate="G$1" pin="DIO2" pad="DIO2"/>
<connect gate="G$1" pin="DIO3" pad="DIO3"/>
<connect gate="G$1" pin="DIO4" pad="DIO4"/>
<connect gate="G$1" pin="DIO5" pad="DIO5"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="GND@1" pad="GND@1"/>
<connect gate="G$1" pin="GND@2" pad="GND@2"/>
<connect gate="G$1" pin="GND@3" pad="GND@3"/>
<connect gate="G$1" pin="MISO" pad="MISO"/>
<connect gate="G$1" pin="MOSI" pad="MOSI"/>
<connect gate="G$1" pin="NSS" pad="NSS"/>
<connect gate="G$1" pin="RST" pad="RST"/>
<connect gate="G$1" pin="SCK" pad="SCK"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SD_CARD_MODULE">
<packages>
<package name="SD_CARD_MODULE">
<pad name="GND" x="5.7462375" y="3.875328125" drill="0.8" diameter="1.4224"/>
<pad name="VCC" x="8.2862375" y="3.875328125" drill="0.8" diameter="1.4224"/>
<pad name="MISO" x="10.8262375" y="3.875328125" drill="0.8" diameter="1.4224"/>
<pad name="MOSI" x="13.3662375" y="3.875328125" drill="0.8" diameter="1.4224"/>
<pad name="SCK" x="15.9062375" y="3.875328125" drill="0.8" diameter="1.4224"/>
<pad name="CS" x="18.4462375" y="3.875328125" drill="0.8" diameter="1.4224"/>
<wire x1="0" y1="0" x2="24" y2="0" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="42.43" width="0.127" layer="21"/>
<wire x1="24" y1="0" x2="24" y2="42.43" width="0.127" layer="21"/>
<wire x1="0" y1="42.43" x2="24" y2="42.43" width="0.127" layer="21"/>
<text x="9.3" y="34.1" size="1.27" layer="27">&gt;VALUE</text>
<text x="9.3" y="31" size="1.27" layer="25">&gt;NAME</text>
<dimension x1="0" y1="42.43" x2="0" y2="0" x3="0" y3="21.215" textsize="1.27" layer="51" dtype="horizontal"/>
<dimension x1="24" y1="0" x2="24.003" y2="4.064" x3="21.33403125" y3="2.03396875" textsize="1.27" layer="51"/>
<dimension x1="5.715" y1="3.937" x2="0.635" y2="3.937" x3="3.175" y3="3.937" textsize="1.27" layer="51"/>
<dimension x1="18.415" y1="3.81" x2="23.495" y2="3.81" x3="20.955" y3="3.81" textsize="1.27" layer="51"/>
<dimension x1="0" y1="42.43" x2="24" y2="42.43" x3="12" y3="42.418" textsize="1.27" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="SD_CARD_MODULE">
<pin name="GND" x="-12.7" y="12.7" length="middle"/>
<pin name="5V" x="-12.7" y="7.62" length="middle"/>
<pin name="MISO" x="-12.7" y="2.54" length="middle"/>
<pin name="MOSI" x="-12.7" y="-2.54" length="middle"/>
<pin name="SCK" x="-12.7" y="-7.62" length="middle"/>
<pin name="CS" x="-12.7" y="-12.7" length="middle"/>
<wire x1="-7.62" y1="15.24" x2="-7.62" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-15.24" x2="7.62" y2="-15.24" width="0.254" layer="94"/>
<wire x1="7.62" y1="-15.24" x2="7.62" y2="15.24" width="0.254" layer="94"/>
<wire x1="7.62" y1="15.24" x2="-7.62" y2="15.24" width="0.254" layer="94"/>
<text x="-5.08" y="20.32" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="17.78" size="1.778" layer="96" align="center">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="SD_CARD_MODULE">
<gates>
<gate name="G$1" symbol="SD_CARD_MODULE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SD_CARD_MODULE">
<connects>
<connect gate="G$1" pin="5V" pad="VCC"/>
<connect gate="G$1" pin="CS" pad="CS"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="MISO" pad="MISO"/>
<connect gate="G$1" pin="MOSI" pad="MOSI"/>
<connect gate="G$1" pin="SCK" pad="SCK"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="MPU_280_MODULE">
<packages>
<package name="MPU_280_MODULE">
<pad name="VIN" x="-2.54" y="9.398" drill="0.8" diameter="1.4224"/>
<pad name="3.3V" x="-2.54" y="6.858" drill="0.8" diameter="1.4224"/>
<pad name="GND" x="-2.54" y="4.318" drill="0.8" diameter="1.4224"/>
<pad name="SCL" x="-2.54" y="1.778" drill="0.8" diameter="1.4224"/>
<pad name="SDA" x="-2.54" y="-0.762" drill="0.8" diameter="1.4224"/>
<pad name="SDO/SAO" x="-2.54" y="-3.302" drill="0.8" diameter="1.4224"/>
<pad name="NCS" x="-2.54" y="-5.842" drill="0.8" diameter="1.4224"/>
<pad name="CSB" x="-2.54" y="-8.382" drill="0.8" diameter="1.4224"/>
<dimension x1="-4" y1="11" x2="11" y2="11" x3="3.5" y3="11" textsize="1.27" layer="51"/>
<dimension x1="11" y1="11" x2="11" y2="-10" x3="11" y3="0.5" textsize="1.27" layer="51"/>
<wire x1="-4" y1="11" x2="11" y2="11" width="0.127" layer="21"/>
<wire x1="11" y1="11" x2="11" y2="-10" width="0.127" layer="21"/>
<wire x1="11" y1="-10" x2="-4" y2="-10" width="0.127" layer="21"/>
<wire x1="-4" y1="-10" x2="-4" y2="11" width="0.127" layer="21"/>
<text x="2.54" y="16.51" size="1.27" layer="25">&gt;NAME</text>
<text x="2.54" y="13.97" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="MPU_280_MODULE">
<pin name="VIN" x="-10.16" y="17.78" length="middle"/>
<pin name="3.3V" x="-10.16" y="12.7" length="middle"/>
<pin name="GND" x="-10.16" y="7.62" length="middle"/>
<pin name="SCL" x="-10.16" y="2.54" length="middle"/>
<pin name="SDA" x="-10.16" y="-2.54" length="middle"/>
<pin name="SDO/SAO" x="-10.16" y="-7.62" length="middle"/>
<pin name="NCS" x="-10.16" y="-12.7" length="middle"/>
<pin name="CSB" x="-10.16" y="-17.78" length="middle"/>
<wire x1="-5.08" y1="20.32" x2="-5.08" y2="-20.32" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-20.32" x2="7.62" y2="-20.32" width="0.254" layer="94"/>
<wire x1="7.62" y1="-20.32" x2="7.62" y2="20.32" width="0.254" layer="94"/>
<wire x1="7.62" y1="20.32" x2="-5.08" y2="20.32" width="0.254" layer="94"/>
<text x="12.7" y="20.32" size="1.27" layer="95">&gt;NAME</text>
<text x="12.7" y="17.78" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="MPU_280_MODULE">
<gates>
<gate name="G$1" symbol="MPU_280_MODULE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MPU_280_MODULE">
<connects>
<connect gate="G$1" pin="3.3V" pad="3.3V"/>
<connect gate="G$1" pin="CSB" pad="CSB"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="NCS" pad="NCS"/>
<connect gate="G$1" pin="SCL" pad="SCL"/>
<connect gate="G$1" pin="SDA" pad="SDA"/>
<connect gate="G$1" pin="SDO/SAO" pad="SDO/SAO"/>
<connect gate="G$1" pin="VIN" pad="VIN"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="NEO_6M_GPS_MODULE">
<packages>
<package name="NEO_6M_GPS_MODULE">
<dimension x1="0" y1="0" x2="24" y2="0" x3="12" y3="-2" textsize="1.27" layer="51"/>
<dimension x1="0" y1="0" x2="1.5" y2="36" x3="-1.2" y3="18" textsize="1" layer="51" dtype="vertical"/>
<wire x1="0" y1="0" x2="24.047" y2="0" width="0.127" layer="21"/>
<wire x1="24.047" y1="0" x2="24.047" y2="36" width="0.127" layer="21"/>
<wire x1="24.047" y1="36" x2="0" y2="36" width="0.127" layer="21"/>
<wire x1="0" y1="36" x2="0" y2="0" width="0.127" layer="21"/>
<pad name="VCC" x="6.985" y="2.032" drill="0.8" diameter="1.4224"/>
<pad name="GND" x="9.525" y="2.032" drill="0.8" diameter="1.4224"/>
<pad name="TXD" x="12.065" y="2.032" drill="0.8" diameter="1.4224"/>
<pad name="RXD" x="14.605" y="2.032" drill="0.8" diameter="1.4224" rot="R90"/>
<pad name="PPS" x="17.145" y="2.032" drill="0.8" diameter="1.4224"/>
<text x="10.16" y="30.48" size="1" layer="25">&gt;NAME</text>
<text x="10.16" y="27.94" size="1" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="NEO_6M_GPS_MODULE">
<pin name="VCC" x="-10.16" y="-5.08" length="middle" rot="R90"/>
<pin name="GND" x="-5.08" y="-5.08" length="middle" rot="R90"/>
<pin name="TXD" x="0" y="-5.08" length="middle" rot="R90"/>
<pin name="RXD" x="5.08" y="-5.08" length="middle" rot="R90"/>
<pin name="PPS" x="10.16" y="-5.08" length="middle" rot="R90"/>
<wire x1="-12.7" y1="0" x2="12.7" y2="0" width="0.254" layer="94"/>
<wire x1="12.7" y1="0" x2="12.7" y2="30.48" width="0.254" layer="94"/>
<wire x1="12.7" y1="30.48" x2="-12.7" y2="30.48" width="0.254" layer="94"/>
<wire x1="-12.7" y1="30.48" x2="-12.7" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="15.24" x2="-2.54" y2="20.32" width="0.254" layer="94"/>
<wire x1="0" y1="15.24" x2="2.54" y2="20.32" width="0.254" layer="94"/>
<wire x1="-2.54" y1="20.32" x2="0" y2="22.86" width="0.254" layer="94" curve="-90"/>
<wire x1="0" y1="22.86" x2="2.54" y2="20.32" width="0.254" layer="94" curve="-90"/>
<circle x="0" y="20.32" radius="0.915809375" width="0.254" layer="94"/>
<text x="-5.08" y="27.94" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="25.4" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="NEO_6M_GPS_MODULE">
<gates>
<gate name="G$1" symbol="NEO_6M_GPS_MODULE" x="0" y="-15.24"/>
</gates>
<devices>
<device name="" package="NEO_6M_GPS_MODULE">
<connects>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="PPS" pad="PPS"/>
<connect gate="G$1" pin="RXD" pad="RXD"/>
<connect gate="G$1" pin="TXD" pad="TXD"/>
<connect gate="G$1" pin="VCC" pad="VCC"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="HC49US">
<description>&lt;Crystals 16MHz 18pF HC49S THROUGH HOLE&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="HC49">
<description>&lt;b&gt;HC49US-16.000MABJ-UB&lt;/b&gt;&lt;br&gt;
</description>
<pad name="1" x="-2.44" y="0" drill="0.8" diameter="1.4224"/>
<pad name="2" x="2.44" y="0" drill="0.8" diameter="1.4224"/>
<text x="-0.45498125" y="-0.00731875" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="-0.45498125" y="-0.00731875" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-3.2" y1="2.325" x2="3.2" y2="2.325" width="0.2" layer="51"/>
<wire x1="-3.2" y1="-2.325" x2="3.2" y2="-2.325" width="0.2" layer="51"/>
<wire x1="-3.2" y1="2.325" x2="-3.2" y2="-2.325" width="0.2" layer="51" curve="180"/>
<wire x1="3.2" y1="2.325" x2="3.2" y2="-2.325" width="0.2" layer="51" curve="-180"/>
<wire x1="3.2" y1="2.325" x2="3.2" y2="-2.325" width="0.2" layer="21" curve="-180"/>
<wire x1="3.2" y1="-2.325" x2="-3.2" y2="-2.325" width="0.2" layer="21"/>
<wire x1="-3.2" y1="-2.325" x2="-3.2" y2="2.325" width="0.2" layer="21" curve="-180"/>
<wire x1="-3.2" y1="2.325" x2="3.2" y2="2.325" width="0.2" layer="21"/>
<circle x="-2.548" y="-2.875" radius="0.043" width="0.2" layer="25"/>
</package>
</packages>
<symbols>
<symbol name="HC49US-16.000MABJ-UB">
<text x="-6.35" y="10.16" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="-3.81" y="7.62" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="1" x="0" y="7.62" length="middle" rot="R270"/>
<pin name="2" x="0" y="-7.62" length="middle" rot="R90"/>
<wire x1="-3.302" y1="1.016" x2="-3.302" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-3.302" y1="-1.016" x2="3.302" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="3.302" y1="-1.016" x2="3.302" y2="1.016" width="0.1524" layer="94"/>
<wire x1="3.302" y1="1.016" x2="-3.302" y2="1.016" width="0.1524" layer="94"/>
<wire x1="3.302" y1="2.54" x2="-3.302" y2="2.54" width="0.1524" layer="94"/>
<wire x1="3.302" y1="-2.54" x2="-3.302" y2="-2.54" width="0.1524" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="HC49US-16.000MABJ-UB" prefix="Y">
<description>&lt;b&gt;Crystals 16MHz 18pF HC49S THROUGH HOLE&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.mouser.com/datasheet/2/77/HC-49_U-S_E-1131675.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="HC49US-16.000MABJ-UB" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HC49">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Crystals 16MHz 18pF HC49S THROUGH HOLE" constant="no"/>
<attribute name="HEIGHT" value="mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Citizen Finetech Miyota" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="HC49US-16.000MABJ-UB" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="695-HC49US-16-U" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.com/Search/Refine.aspx?Keyword=695-HC49US-16-U" constant="no"/>
<attribute name="RS_PART_NUMBER" value="" constant="no"/>
<attribute name="RS_PRICE-STOCK" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="DHT11_MODULE">
<packages>
<package name="DHT11_MODULE">
<pad name="GND" x="-2.44" y="3.84" drill="0.8" diameter="1.4224"/>
<pad name="DATA" x="-2.44" y="1.3" drill="0.8" diameter="1.4224"/>
<pad name="VCC" x="-2.44" y="-1.24" drill="0.8" diameter="1.4224"/>
<dimension x1="-5.08" y1="-5.08" x2="25" y2="-5.1" x3="9.96" y3="-5.09" textsize="1.27" layer="51"/>
<dimension x1="-5.08" y1="-5.08" x2="-5.1" y2="8" x3="-5.4" y3="1.46" textsize="1.27" layer="51" dtype="vertical"/>
<dimension x1="-2.5" y1="-1.3" x2="-5.5" y2="-2.8" x3="-4" y3="-2.8" textsize="1.27" layer="51" dtype="horizontal"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="8" width="0.127" layer="21"/>
<wire x1="-5.08" y1="8" x2="-5.1" y2="8" width="0.127" layer="21"/>
<wire x1="-5.08" y1="8" x2="-2.6" y2="8" width="0.127" layer="21"/>
<wire x1="-5.4" y1="-2.6" x2="-5.4" y2="-5.1" width="0.127" layer="21"/>
<wire x1="-5.4" y1="-5.1" x2="-2.5" y2="-5.1" width="0.127" layer="21"/>
<wire x1="25" y1="-3.2" x2="25" y2="-5.1" width="0.127" layer="21"/>
<wire x1="25" y1="-5.1" x2="23.5" y2="-5.1" width="0.127" layer="21"/>
<wire x1="24.9" y1="6.7" x2="24.9" y2="7.9" width="0.127" layer="21"/>
<wire x1="24.9" y1="7.9" x2="23.3" y2="7.9" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="DHT11_MODULE">
<pin name="GND" x="-5.08" y="5.08" length="middle"/>
<pin name="DATA" x="-5.08" y="0" length="middle"/>
<pin name="VCC" x="-5.08" y="-5.08" length="middle"/>
<wire x1="0" y1="7.62" x2="0" y2="-7.62" width="0.254" layer="94"/>
<wire x1="0" y1="-7.62" x2="22.86" y2="-7.62" width="0.254" layer="94"/>
<wire x1="22.86" y1="-7.62" x2="22.86" y2="7.62" width="0.254" layer="94"/>
<wire x1="22.86" y1="7.62" x2="0" y2="7.62" width="0.254" layer="94"/>
<wire x1="20.32" y1="5.08" x2="20.32" y2="2.54" width="0.254" layer="94"/>
<wire x1="20.32" y1="2.54" x2="20.32" y2="0" width="0.254" layer="94"/>
<wire x1="20.32" y1="0" x2="20.32" y2="-2.54" width="0.254" layer="94"/>
<wire x1="20.32" y1="-2.54" x2="20.32" y2="-5.08" width="0.254" layer="94"/>
<wire x1="20.32" y1="-5.08" x2="17.78" y2="-5.08" width="0.254" layer="94"/>
<wire x1="17.78" y1="-5.08" x2="15.24" y2="-5.08" width="0.254" layer="94"/>
<wire x1="15.24" y1="-5.08" x2="12.7" y2="-5.08" width="0.254" layer="94"/>
<wire x1="12.7" y1="-5.08" x2="10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="7.62" y2="0" width="0.254" layer="94"/>
<wire x1="7.62" y1="0" x2="7.62" y2="2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="2.54" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="10.16" y2="5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="5.08" x2="12.7" y2="5.08" width="0.254" layer="94"/>
<wire x1="12.7" y1="5.08" x2="15.24" y2="5.08" width="0.254" layer="94"/>
<wire x1="15.24" y1="5.08" x2="17.78" y2="5.08" width="0.254" layer="94"/>
<wire x1="17.78" y1="5.08" x2="20.32" y2="5.08" width="0.254" layer="94"/>
<wire x1="17.78" y1="2.54" x2="17.78" y2="-2.54" width="0.254" layer="94"/>
<wire x1="17.78" y1="-2.54" x2="10.16" y2="-2.54" width="0.254" layer="94"/>
<wire x1="10.16" y1="-2.54" x2="10.16" y2="2.54" width="0.254" layer="94"/>
<wire x1="10.16" y1="2.54" x2="17.78" y2="2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="0" x2="20.32" y2="0" width="0.254" layer="94"/>
<wire x1="17.78" y1="5.08" x2="17.78" y2="-5.08" width="0.254" layer="94"/>
<wire x1="15.24" y1="5.08" x2="15.24" y2="-5.08" width="0.254" layer="94"/>
<wire x1="12.7" y1="5.08" x2="12.7" y2="-5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="5.08" x2="10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="2.54" x2="20.32" y2="2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="20.32" y2="-2.54" width="0.254" layer="94"/>
<text x="7.62" y="12.7" size="1.778" layer="95">&gt;NAME</text>
<text x="7.62" y="10.16" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="DHT11_MODULE">
<gates>
<gate name="G$1" symbol="DHT11_MODULE" x="-5.08" y="0"/>
</gates>
<devices>
<device name="" package="DHT11_MODULE">
<connects>
<connect gate="G$1" pin="DATA" pad="DATA"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VCC" pad="VCC"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="PIN_HEADERS">
<packages>
<package name="2X4">
<pad name="P$1" x="-2.54" y="3.81" drill="0.8" diameter="1.4224"/>
<pad name="P$2" x="-2.54" y="1.27" drill="0.8" diameter="1.4224"/>
<pad name="P$3" x="-2.54" y="-1.27" drill="0.8" diameter="1.4224"/>
<pad name="P$4" x="-2.54" y="-3.81" drill="0.8" diameter="1.4224"/>
<pad name="P$5" x="2.54" y="-3.81" drill="0.8" diameter="1.4224"/>
<pad name="P$6" x="2.54" y="-1.27" drill="0.8" diameter="1.4224"/>
<pad name="P$7" x="2.54" y="1.27" drill="0.8" diameter="1.4224"/>
<pad name="P$8" x="2.54" y="3.81" drill="0.8" diameter="1.4224"/>
<text x="0" y="5.08" size="1.27" layer="25">&gt;NAME</text>
<text x="0" y="6.62" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-4" y1="5" x2="4" y2="5" width="0.127" layer="21"/>
<wire x1="4" y1="5" x2="4" y2="-5" width="0.127" layer="21"/>
<wire x1="4" y1="-5" x2="-4" y2="-5" width="0.127" layer="21"/>
<wire x1="-4" y1="-5" x2="-4" y2="5" width="0.127" layer="21"/>
</package>
<package name="O">
<pad name="O" x="0" y="0" drill="0.8" diameter="1.4" shape="square"/>
</package>
</packages>
<symbols>
<symbol name="2X4">
<pin name="P$1" x="-12.7" y="7.62" length="middle"/>
<pin name="P$2" x="-12.7" y="2.54" length="middle"/>
<pin name="P$3" x="-12.7" y="-2.54" length="middle"/>
<pin name="P$4" x="-12.7" y="-7.62" length="middle"/>
<pin name="P$5" x="12.7" y="-7.62" length="middle" rot="R180"/>
<pin name="P$6" x="12.7" y="-2.54" length="middle" rot="R180"/>
<pin name="P$7" x="12.7" y="2.54" length="middle" rot="R180"/>
<pin name="P$8" x="12.7" y="7.62" length="middle" rot="R180"/>
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-10.16" x2="7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="-10.16" x2="7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="10.16" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
<text x="-1.524" y="11.938" size="1.27" layer="95" rot="MR180">&gt;NAME</text>
<text x="-1.524" y="12.954" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="O">
<pin name="O" x="0" y="0" length="point"/>
<circle x="0" y="0" radius="1.016" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="2X4">
<gates>
<gate name="2X4" symbol="2X4" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2X4">
<connects>
<connect gate="2X4" pin="P$1" pad="P$1"/>
<connect gate="2X4" pin="P$2" pad="P$2"/>
<connect gate="2X4" pin="P$3" pad="P$3"/>
<connect gate="2X4" pin="P$4" pad="P$4"/>
<connect gate="2X4" pin="P$5" pad="P$5"/>
<connect gate="2X4" pin="P$6" pad="P$6"/>
<connect gate="2X4" pin="P$7" pad="P$7"/>
<connect gate="2X4" pin="P$8" pad="P$8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="O">
<gates>
<gate name="G$1" symbol="O" x="0" y="0"/>
</gates>
<devices>
<device name="" package="O">
<connects>
<connect gate="G$1" pin="O" pad="O"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="AMS1117-3.3V">
<description>&lt;1A LOW DROPOUT VOLTAGE REGULATOR, SOT-223&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="SOT229P700X180-4N">
<description>&lt;b&gt;3 Lead SOT-223 Plastic Package&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-3.35" y="2.29" dx="1.3" dy="0.95" layer="1"/>
<smd name="2" x="-3.35" y="0" dx="1.3" dy="0.95" layer="1"/>
<smd name="3" x="-3.35" y="-2.29" dx="1.3" dy="0.95" layer="1"/>
<smd name="4" x="3.35" y="0" dx="3.25" dy="1.3" layer="1" rot="R90"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-4.25" y1="3.605" x2="4.25" y2="3.605" width="0.05" layer="51"/>
<wire x1="4.25" y1="3.605" x2="4.25" y2="-3.605" width="0.05" layer="51"/>
<wire x1="4.25" y1="-3.605" x2="-4.25" y2="-3.605" width="0.05" layer="51"/>
<wire x1="-4.25" y1="-3.605" x2="-4.25" y2="3.605" width="0.05" layer="51"/>
<wire x1="-1.752" y1="3.252" x2="1.752" y2="3.252" width="0.1" layer="51"/>
<wire x1="1.752" y1="3.252" x2="1.752" y2="-3.252" width="0.1" layer="51"/>
<wire x1="1.752" y1="-3.252" x2="-1.752" y2="-3.252" width="0.1" layer="51"/>
<wire x1="-1.752" y1="-3.252" x2="-1.752" y2="3.252" width="0.1" layer="51"/>
<wire x1="-1.752" y1="0.962" x2="0.538" y2="3.252" width="0.1" layer="51"/>
<wire x1="-1.752" y1="3.252" x2="1.752" y2="3.252" width="0.2" layer="21"/>
<wire x1="1.752" y1="3.252" x2="1.752" y2="-3.252" width="0.2" layer="21"/>
<wire x1="1.752" y1="-3.252" x2="-1.752" y2="-3.252" width="0.2" layer="21"/>
<wire x1="-1.752" y1="-3.252" x2="-1.752" y2="3.252" width="0.2" layer="21"/>
<wire x1="-4" y1="3.115" x2="-2.7" y2="3.115" width="0.2" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="AMS1117-3.3V">
<wire x1="5.08" y1="10.16" x2="30.48" y2="10.16" width="0.254" layer="94"/>
<wire x1="30.48" y1="-10.16" x2="30.48" y2="-5.08" width="0.254" layer="94"/>
<wire x1="30.48" y1="-5.08" x2="30.48" y2="5.08" width="0.254" layer="94"/>
<wire x1="30.48" y1="5.08" x2="30.48" y2="10.16" width="0.254" layer="94"/>
<wire x1="30.48" y1="-10.16" x2="5.08" y2="-10.16" width="0.254" layer="94"/>
<wire x1="5.08" y1="10.16" x2="5.08" y2="-10.16" width="0.254" layer="94"/>
<text x="46.99" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="46.99" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="GROUND/ADJUST" x="0" y="5.08" length="middle"/>
<pin name="VOUT" x="0" y="0" length="middle"/>
<pin name="VIN" x="0" y="-5.08" length="middle"/>
<pin name="TAB_(OUTPUT)" x="40.64" y="0" length="middle" rot="R180"/>
<wire x1="30.48" y1="5.08" x2="35.56" y2="5.08" width="0.254" layer="94"/>
<wire x1="35.56" y1="5.08" x2="35.56" y2="-5.08" width="0.254" layer="94"/>
<wire x1="35.56" y1="-5.08" x2="30.48" y2="-5.08" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="AMS1117-3.3V" prefix="IC">
<description>&lt;b&gt;1A LOW DROPOUT VOLTAGE REGULATOR, SOT-223&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.advanced-monolithic.com/pdf/ds1117.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="AMS1117-3.3V" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT229P700X180-4N">
<connects>
<connect gate="G$1" pin="GROUND/ADJUST" pad="1"/>
<connect gate="G$1" pin="TAB_(OUTPUT)" pad="4"/>
<connect gate="G$1" pin="VIN" pad="3"/>
<connect gate="G$1" pin="VOUT" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="1A LOW DROPOUT VOLTAGE REGULATOR, SOT-223" constant="no"/>
<attribute name="HEIGHT" value="1.8mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Advanced Monolithic Systems" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="ams1117-3.3v" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="" constant="no"/>
<attribute name="RS_PART_NUMBER" value="" constant="no"/>
<attribute name="RS_PRICE-STOCK" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SMD_RESISTORS">
<description>&lt;Thick Film Resistors - SMD 2.49K OHM 1%&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="R0805">
<smd name="1" x="-1" y="0" dx="1.45" dy="0.95" layer="1" rot="R90"/>
<smd name="2" x="1" y="0" dx="1.45" dy="0.95" layer="1" rot="R90"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-1.725" y1="1" x2="1.725" y2="1" width="0.05" layer="51"/>
<wire x1="1.725" y1="1" x2="1.725" y2="-1" width="0.05" layer="51"/>
<wire x1="1.725" y1="-1" x2="-1.725" y2="-1" width="0.05" layer="51"/>
<wire x1="-1.725" y1="-1" x2="-1.725" y2="1" width="0.05" layer="51"/>
<wire x1="-1.025" y1="0.625" x2="1.025" y2="0.625" width="0.1" layer="51"/>
<wire x1="1.025" y1="0.625" x2="1.025" y2="-0.625" width="0.1" layer="51"/>
<wire x1="1.025" y1="-0.625" x2="-1.025" y2="-0.625" width="0.1" layer="51"/>
<wire x1="-1.025" y1="-0.625" x2="-1.025" y2="0.625" width="0.1" layer="51"/>
<wire x1="0" y1="0.525" x2="0" y2="-0.525" width="0.2" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="R0805">
<wire x1="-5.08" y1="1.27" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="-5.08" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="-1.27" width="0.254" layer="94"/>
<text x="3.81" y="6.35" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="3.81" y="3.81" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="1" x="-10.16" y="0" visible="pad" length="middle"/>
<pin name="2" x="7.62" y="0" visible="pad" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="R0805">
<gates>
<gate name="G$1" symbol="R0805" x="0" y="0"/>
</gates>
<devices>
<device name="" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
<class number="2" name="New Class" width="0" drill="0">
</class>
</classes>
<parts>
<part name="MASTER" library="ATMEGA328P-AU" deviceset="ATMEGA328P-AU" device=""/>
<part name="SLAVE" library="ATMEGA328P-AU" deviceset="ATMEGA328P-AU" device=""/>
<part name="P+1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="P+6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="R1" library="R1206" deviceset="RC1206FR-072K49L" device="" value="10K"/>
<part name="R2" library="R1206" deviceset="RC1206FR-072K49L" device="" value="10K"/>
<part name="P+3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="P+5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="C1" library="C1206" deviceset="12065D106KAT2A" device="" value="0.1uF"/>
<part name="C2" library="C1206" deviceset="12065D106KAT2A" device="" value="0.1uF"/>
<part name="P+7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="U$2" library="LORA_MODULE" deviceset="LORA_MODULE" device=""/>
<part name="U$3" library="SD_CARD_MODULE" deviceset="SD_CARD_MODULE" device=""/>
<part name="U$5" library="MPU_280_MODULE" deviceset="MPU_280_MODULE" device=""/>
<part name="U$4" library="NEO_6M_GPS_MODULE" deviceset="NEO_6M_GPS_MODULE" device=""/>
<part name="Y1" library="HC49US" deviceset="HC49US-16.000MABJ-UB" device="" value="16Mhz"/>
<part name="Y2" library="HC49US" deviceset="HC49US-16.000MABJ-UB" device="" value="16Mhz"/>
<part name="U$6" library="2x4" deviceset="2X3" device="" value="SPI_MASTER"/>
<part name="U$7" library="2x4" deviceset="2X3" device="" value="SPI_SLAVE"/>
<part name="P+2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="P+4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="U$8" library="DHT11_MODULE" deviceset="DHT11_MODULE" device=""/>
<part name="GND7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="U$9" library="PIN_HEADERS" deviceset="2X4" device="" value="UART/BOOTLOADER_PROG"/>
<part name="GND11" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND12" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="IC1" library="AMS1117-3.3V" deviceset="AMS1117-3.3V" device=""/>
<part name="GND13" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+11" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="+3V1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="P+12" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND14" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="." library="PIN_HEADERS" deviceset="O" device="" value="O"/>
<part name=".@1" library="PIN_HEADERS" deviceset="O" device=""/>
<part name="U$15" library="PIN_HEADERS" deviceset="O" device=""/>
<part name="U$16" library="PIN_HEADERS" deviceset="O" device=""/>
<part name="U$17" library="PIN_HEADERS" deviceset="O" device=""/>
<part name="U$18" library="PIN_HEADERS" deviceset="O" device=""/>
<part name="U$19" library="PIN_HEADERS" deviceset="O" device=""/>
<part name="U$20" library="PIN_HEADERS" deviceset="O" device=""/>
<part name="U$21" library="PIN_HEADERS" deviceset="O" device=""/>
<part name="U$11" library="PIN_HEADERS" deviceset="O" device=""/>
<part name="U$22" library="PIN_HEADERS" deviceset="O" device=""/>
<part name="U$24" library="PIN_HEADERS" deviceset="O" device=""/>
<part name="U$25" library="PIN_HEADERS" deviceset="O" device=""/>
<part name="U$26" library="PIN_HEADERS" deviceset="O" device=""/>
<part name="U$27" library="PIN_HEADERS" deviceset="O" device=""/>
<part name="U$28" library="PIN_HEADERS" deviceset="O" device=""/>
<part name="U$29" library="PIN_HEADERS" deviceset="O" device=""/>
<part name="U$30" library="PIN_HEADERS" deviceset="O" device=""/>
<part name="U$31" library="PIN_HEADERS" deviceset="O" device=""/>
<part name="U$32" library="PIN_HEADERS" deviceset="O" device=""/>
<part name="U$23" library="PIN_HEADERS" deviceset="O" device=""/>
<part name="R3" library="SMD_RESISTORS" deviceset="R0805" device="" value="0R"/>
<part name="R4" library="SMD_RESISTORS" deviceset="R0805" device="" value="0R"/>
<part name="R5" library="SMD_RESISTORS" deviceset="R0805" device="" value="0R"/>
<part name="R6" library="SMD_RESISTORS" deviceset="R0805" device="" value="0R"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="MASTER" gate="G$1" x="-76.2" y="-160.02" smashed="yes">
<attribute name="NAME" x="-29.21" y="-124.46" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="-29.21" y="-127" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="SLAVE" gate="G$1" x="50.8" y="-165.1" smashed="yes">
<attribute name="NAME" x="97.79" y="-129.54" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="97.79" y="-132.08" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="P+1" gate="VCC" x="-83.82" y="-165.1" smashed="yes">
<attribute name="VALUE" x="-86.36" y="-167.64" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+6" gate="VCC" x="38.1" y="-170.18" smashed="yes">
<attribute name="VALUE" x="35.56" y="-172.72" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R1" gate="G$1" x="-55.88" y="-101.6" smashed="yes" rot="R90">
<attribute name="NAME" x="-54.61" y="-87.63" size="1.778" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="-55.88" y="-95.25" size="1.778" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="R2" gate="G$1" x="73.66" y="-106.68" smashed="yes" rot="R90">
<attribute name="NAME" x="76.2" y="-99.06" size="1.778" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="73.66" y="-109.982" size="1.778" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="P+3" gate="VCC" x="73.66" y="-81.28" smashed="yes">
<attribute name="VALUE" x="71.12" y="-83.82" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+5" gate="VCC" x="-55.88" y="-83.82" smashed="yes">
<attribute name="VALUE" x="-58.42" y="-86.36" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C1" gate="G$1" x="83.82" y="-106.68" smashed="yes" rot="R90">
<attribute name="NAME" x="85.09" y="-103.124" size="1.778" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="81.026" y="-102.87" size="1.778" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="C2" gate="G$1" x="-45.72" y="-106.68" smashed="yes" rot="R90">
<attribute name="NAME" x="-47.752" y="-106.426" size="1.778" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="-43.942" y="-106.934" size="1.778" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="P+7" gate="VCC" x="195.58" y="-45.72" smashed="yes" rot="R90">
<attribute name="VALUE" x="198.12" y="-48.26" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND1" gate="1" x="238.76" y="-48.26" smashed="yes">
<attribute name="VALUE" x="236.22" y="-50.8" size="1.778" layer="96"/>
</instance>
<instance part="GND2" gate="1" x="111.76" y="-175.26" smashed="yes">
<attribute name="VALUE" x="109.22" y="-177.8" size="1.778" layer="96"/>
</instance>
<instance part="GND5" gate="1" x="71.12" y="-175.26" smashed="yes" rot="R90">
<attribute name="VALUE" x="73.66" y="-177.8" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND8" gate="1" x="-15.24" y="-170.18" smashed="yes">
<attribute name="VALUE" x="-17.78" y="-172.72" size="1.778" layer="96"/>
</instance>
<instance part="GND9" gate="1" x="-55.88" y="-167.64" smashed="yes" rot="R90">
<attribute name="VALUE" x="-53.34" y="-170.18" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="U$2" gate="G$1" x="-182.88" y="-162.56" smashed="yes">
<attribute name="NAME" x="-185.42" y="-137.16" size="1.27" layer="95"/>
<attribute name="VALUE" x="-185.42" y="-139.7" size="1.27" layer="96"/>
</instance>
<instance part="U$3" gate="G$1" x="167.64" y="-233.68" smashed="yes">
<attribute name="NAME" x="162.56" y="-213.36" size="1.778" layer="95"/>
<attribute name="VALUE" x="162.56" y="-215.9" size="1.778" layer="96" align="center"/>
</instance>
<instance part="U$5" gate="G$1" x="172.72" y="-177.8" smashed="yes">
<attribute name="NAME" x="185.42" y="-157.48" size="1.27" layer="95"/>
<attribute name="VALUE" x="185.42" y="-160.02" size="1.27" layer="96"/>
</instance>
<instance part="U$4" gate="G$1" x="35.56" y="-284.48" smashed="yes" rot="R90">
<attribute name="NAME" x="7.62" y="-289.56" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="10.16" y="-289.56" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="Y1" gate="G$1" x="30.48" y="-185.42" smashed="yes">
<attribute name="NAME" x="24.13" y="-175.26" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="21.59" y="-177.8" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="Y2" gate="G$1" x="-99.06" y="-175.26" smashed="yes">
<attribute name="NAME" x="-105.41" y="-165.1" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="-107.95" y="-167.64" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="U$6" gate="G$1" x="-63.5" y="-27.94" smashed="yes">
<attribute name="NAME" x="-65.024" y="-18.542" size="1.27" layer="95" rot="MR180"/>
<attribute name="VALUE" x="-65.024" y="-17.526" size="1.27" layer="96"/>
</instance>
<instance part="U$7" gate="G$1" x="60.96" y="-30.48" smashed="yes">
<attribute name="NAME" x="59.436" y="-21.082" size="1.27" layer="95" rot="MR180"/>
<attribute name="VALUE" x="59.436" y="-20.066" size="1.27" layer="96"/>
</instance>
<instance part="P+2" gate="VCC" x="-43.18" y="-12.7" smashed="yes">
<attribute name="VALUE" x="-45.72" y="-15.24" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+4" gate="VCC" x="81.28" y="-20.32" smashed="yes">
<attribute name="VALUE" x="78.74" y="-22.86" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND3" gate="1" x="-40.64" y="-38.1" smashed="yes">
<attribute name="VALUE" x="-43.18" y="-40.64" size="1.778" layer="96"/>
</instance>
<instance part="GND4" gate="1" x="83.82" y="-40.64" smashed="yes">
<attribute name="VALUE" x="81.28" y="-43.18" size="1.778" layer="96"/>
</instance>
<instance part="P+8" gate="VCC" x="139.7" y="-154.94" smashed="yes">
<attribute name="VALUE" x="137.16" y="-157.48" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND6" gate="1" x="144.78" y="-170.18" smashed="yes" rot="R270">
<attribute name="VALUE" x="142.24" y="-167.64" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="U$8" gate="G$1" x="40.64" y="-106.68" smashed="yes" rot="R90">
<attribute name="NAME" x="27.94" y="-99.06" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="30.48" y="-99.06" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND7" gate="1" x="35.56" y="-119.38" smashed="yes">
<attribute name="VALUE" x="33.02" y="-121.92" size="1.778" layer="96"/>
</instance>
<instance part="P+9" gate="VCC" x="53.34" y="-106.68" smashed="yes">
<attribute name="VALUE" x="55.88" y="-106.68" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND10" gate="1" x="139.7" y="-220.98" smashed="yes" rot="R270">
<attribute name="VALUE" x="137.16" y="-218.44" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="P+10" gate="VCC" x="129.54" y="-220.98" smashed="yes">
<attribute name="VALUE" x="127" y="-223.52" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="U$9" gate="2X4" x="218.44" y="-38.1" smashed="yes">
<attribute name="NAME" x="216.916" y="-26.162" size="1.27" layer="95" rot="MR180"/>
<attribute name="VALUE" x="216.916" y="-25.146" size="1.27" layer="96"/>
</instance>
<instance part="GND11" gate="1" x="-180.34" y="-187.96" smashed="yes">
<attribute name="VALUE" x="-182.88" y="-190.5" size="1.778" layer="96"/>
</instance>
<instance part="GND12" gate="1" x="-220.98" y="-144.78" smashed="yes">
<attribute name="VALUE" x="-223.52" y="-147.32" size="1.778" layer="96"/>
</instance>
<instance part="IC1" gate="G$1" x="-172.72" y="-251.46" smashed="yes">
<attribute name="NAME" x="-125.73" y="-243.84" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="-125.73" y="-246.38" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="GND13" gate="1" x="-185.42" y="-246.38" smashed="yes" rot="R270">
<attribute name="VALUE" x="-187.96" y="-243.84" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="P+11" gate="VCC" x="-182.88" y="-254" smashed="yes">
<attribute name="VALUE" x="-185.42" y="-256.54" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="+3V1" gate="G$1" x="-157.48" y="-228.6" smashed="yes">
<attribute name="VALUE" x="-160.02" y="-233.68" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="+3V2" gate="G$1" x="-132.08" y="-167.64" smashed="yes" rot="R270">
<attribute name="VALUE" x="-137.16" y="-165.1" size="1.778" layer="96"/>
</instance>
<instance part="P+12" gate="VCC" x="50.8" y="-299.72" smashed="yes" rot="R270">
<attribute name="VALUE" x="50.038" y="-297.942" size="1.778" layer="96"/>
</instance>
<instance part="GND14" gate="1" x="48.26" y="-292.1" smashed="yes">
<attribute name="VALUE" x="48.768" y="-291.592" size="1.778" layer="96"/>
</instance>
<instance part="+3V3" gate="G$1" x="154.94" y="-165.1" smashed="yes" rot="R90">
<attribute name="VALUE" x="154.94" y="-162.56" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="." gate="G$1" x="-81.28" y="-160.02" smashed="yes">
<attribute name="NAME" x="-81.28" y="-157.48" size="1.778" layer="95"/>
<attribute name="VALUE" x="-81.28" y="-160.02" size="1.778" layer="96"/>
</instance>
<instance part=".@1" gate="G$1" x="-81.28" y="-162.56" smashed="yes"/>
<instance part="U$15" gate="G$1" x="-20.32" y="-165.1" smashed="yes">
<attribute name="NAME" x="-20.32" y="-165.1" size="0.508" layer="95" align="center"/>
<attribute name="VALUE" x="-20.32" y="-165.1" size="0.508" layer="96" align="center"/>
</instance>
<instance part="U$16" gate="G$1" x="-20.32" y="-170.18" smashed="yes">
<attribute name="NAME" x="-20.32" y="-170.18" size="0.508" layer="95" align="center"/>
<attribute name="VALUE" x="-20.32" y="-170.18" size="0.508" layer="96" align="center"/>
</instance>
<instance part="U$17" gate="G$1" x="-20.32" y="-172.72" smashed="yes">
<attribute name="NAME" x="-20.32" y="-172.72" size="0.508" layer="95" align="center"/>
<attribute name="VALUE" x="-20.32" y="-172.72" size="0.508" layer="96" align="center"/>
</instance>
<instance part="U$18" gate="G$1" x="-58.42" y="-208.28" smashed="yes">
<attribute name="NAME" x="-58.42" y="-208.28" size="0.508" layer="95" align="center"/>
<attribute name="VALUE" x="-58.42" y="-208.28" size="0.508" layer="96" align="center"/>
</instance>
<instance part="U$19" gate="G$1" x="-55.88" y="-208.28" smashed="yes">
<attribute name="NAME" x="-55.88" y="-208.28" size="0.508" layer="95" align="center"/>
<attribute name="VALUE" x="-55.88" y="-208.28" size="0.508" layer="96" align="center"/>
</instance>
<instance part="U$20" gate="G$1" x="-50.8" y="-208.28" smashed="yes">
<attribute name="NAME" x="-50.8" y="-208.28" size="0.508" layer="95" align="center"/>
<attribute name="VALUE" x="-50.8" y="-208.28" size="0.508" layer="96" align="center"/>
</instance>
<instance part="U$21" gate="G$1" x="-53.34" y="-208.28" smashed="yes">
<attribute name="NAME" x="-53.34" y="-208.28" size="0.508" layer="95" align="center"/>
<attribute name="VALUE" x="-53.34" y="-208.28" size="0.508" layer="96" align="center"/>
</instance>
<instance part="U$11" gate="G$1" x="45.72" y="-167.64" smashed="yes">
<attribute name="NAME" x="45.72" y="-167.64" size="0.508" layer="95" align="center"/>
<attribute name="VALUE" x="45.72" y="-167.64" size="0.508" layer="96" align="center"/>
</instance>
<instance part="U$22" gate="G$1" x="45.72" y="-165.1" smashed="yes">
<attribute name="NAME" x="45.72" y="-165.1" size="0.508" layer="95" align="center"/>
<attribute name="VALUE" x="45.72" y="-165.1" size="0.508" layer="96" align="center"/>
</instance>
<instance part="U$24" gate="G$1" x="81.28" y="-127" smashed="yes">
<attribute name="NAME" x="81.28" y="-127" size="0.508" layer="95" align="center"/>
<attribute name="VALUE" x="81.28" y="-127" size="0.508" layer="96" align="center"/>
</instance>
<instance part="U$25" gate="G$1" x="83.82" y="-127" smashed="yes">
<attribute name="NAME" x="83.82" y="-127" size="0.508" layer="95" align="center"/>
<attribute name="VALUE" x="83.82" y="-127" size="0.508" layer="96" align="center"/>
</instance>
<instance part="U$26" gate="G$1" x="106.68" y="-165.1" smashed="yes">
<attribute name="NAME" x="106.68" y="-165.1" size="0.508" layer="95" align="center"/>
<attribute name="VALUE" x="106.68" y="-165.1" size="0.508" layer="96" align="center"/>
</instance>
<instance part="U$27" gate="G$1" x="106.68" y="-167.64" smashed="yes">
<attribute name="NAME" x="106.68" y="-167.64" size="0.508" layer="95" align="center"/>
<attribute name="VALUE" x="106.68" y="-167.64" size="0.508" layer="96" align="center"/>
</instance>
<instance part="U$28" gate="G$1" x="106.68" y="-170.18" smashed="yes">
<attribute name="NAME" x="106.68" y="-170.18" size="0.508" layer="95" align="center"/>
<attribute name="VALUE" x="106.68" y="-170.18" size="0.508" layer="96" align="center"/>
</instance>
<instance part="U$29" gate="G$1" x="106.68" y="-175.26" smashed="yes">
<attribute name="NAME" x="106.68" y="-175.26" size="0.508" layer="95" align="center"/>
<attribute name="VALUE" x="106.68" y="-175.26" size="0.508" layer="96" align="center"/>
</instance>
<instance part="U$30" gate="G$1" x="106.68" y="-177.8" smashed="yes">
<attribute name="NAME" x="106.68" y="-177.8" size="0.508" layer="95" align="center"/>
<attribute name="VALUE" x="106.68" y="-177.8" size="0.508" layer="96" align="center"/>
</instance>
<instance part="U$31" gate="G$1" x="68.58" y="-213.36" smashed="yes">
<attribute name="NAME" x="68.58" y="-213.36" size="0.508" layer="95" align="center"/>
<attribute name="VALUE" x="68.58" y="-213.36" size="0.508" layer="96" align="center"/>
</instance>
<instance part="U$32" gate="G$1" x="66.04" y="-213.36" smashed="yes">
<attribute name="NAME" x="66.04" y="-213.36" size="0.508" layer="95" align="center"/>
<attribute name="VALUE" x="66.04" y="-213.36" size="0.508" layer="96" align="center"/>
</instance>
<instance part="U$23" gate="G$1" x="76.2" y="-213.36" smashed="yes"/>
<instance part="R3" gate="G$1" x="68.58" y="-226.06" smashed="yes" rot="R90">
<attribute name="NAME" x="65.786" y="-226.568" size="1.778" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="66.04" y="-230.886" size="1.778" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="R4" gate="G$1" x="73.66" y="-226.06" smashed="yes" rot="R90">
<attribute name="NAME" x="75.184" y="-225.298" size="1.778" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="75.438" y="-229.616" size="1.778" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="R5" gate="G$1" x="-5.08" y="-162.56" smashed="yes">
<attribute name="NAME" x="-7.874" y="-159.766" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="-4.064" y="-159.766" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="R6" gate="G$1" x="-5.08" y="-154.94" smashed="yes">
<attribute name="NAME" x="-7.366" y="-152.4" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="-3.048" y="-152.4" size="1.778" layer="96" align="center-left"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="231.14" y1="-45.72" x2="238.76" y2="-45.72" width="0.1524" layer="91"/>
<pinref part="U$9" gate="2X4" pin="P$5"/>
</segment>
<segment>
<pinref part="SLAVE" gate="G$1" pin="GND_2"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="50.8" y1="-175.26" x2="68.58" y2="-175.26" width="0.1524" layer="91"/>
<pinref part="SLAVE" gate="G$1" pin="GND_1"/>
<wire x1="50.8" y1="-170.18" x2="68.58" y2="-170.18" width="0.1524" layer="91"/>
<wire x1="68.58" y1="-170.18" x2="68.58" y2="-175.26" width="0.1524" layer="91"/>
<junction x="68.58" y="-175.26"/>
</segment>
<segment>
<pinref part="SLAVE" gate="G$1" pin="GND_3"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="101.6" y1="-172.72" x2="111.76" y2="-172.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="MASTER" gate="G$1" pin="GND_1"/>
<wire x1="-76.2" y1="-165.1" x2="-58.42" y2="-165.1" width="0.1524" layer="91"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="-58.42" y1="-165.1" x2="-58.42" y2="-167.64" width="0.1524" layer="91"/>
<pinref part="MASTER" gate="G$1" pin="GND_2"/>
<wire x1="-76.2" y1="-170.18" x2="-58.42" y2="-170.18" width="0.1524" layer="91"/>
<wire x1="-58.42" y1="-170.18" x2="-58.42" y2="-167.64" width="0.1524" layer="91"/>
<junction x="-58.42" y="-167.64"/>
</segment>
<segment>
<pinref part="MASTER" gate="G$1" pin="GND_3"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="-25.4" y1="-167.64" x2="-15.24" y2="-167.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="P$6"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="-50.8" y1="-33.02" x2="-40.64" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="-33.02" x2="-40.64" y2="-35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$7" gate="G$1" pin="P$6"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="73.66" y1="-35.56" x2="83.82" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="83.82" y1="-35.56" x2="83.82" y2="-38.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND6" gate="1" pin="GND"/>
<pinref part="U$5" gate="G$1" pin="GND"/>
<wire x1="147.32" y1="-170.18" x2="149.86" y2="-170.18" width="0.1524" layer="91"/>
<pinref part="U$5" gate="G$1" pin="SDO/SAO"/>
<wire x1="149.86" y1="-170.18" x2="162.56" y2="-170.18" width="0.1524" layer="91"/>
<wire x1="162.56" y1="-185.42" x2="149.86" y2="-185.42" width="0.1524" layer="91"/>
<wire x1="149.86" y1="-185.42" x2="149.86" y2="-170.18" width="0.1524" layer="91"/>
<junction x="149.86" y="-170.18"/>
</segment>
<segment>
<pinref part="GND7" gate="1" pin="GND"/>
<pinref part="U$8" gate="G$1" pin="GND"/>
<wire x1="35.56" y1="-116.84" x2="35.56" y2="-111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND10" gate="1" pin="GND"/>
<pinref part="U$3" gate="G$1" pin="GND"/>
<wire x1="142.24" y1="-220.98" x2="154.94" y2="-220.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="GND@1"/>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="-198.12" y1="-177.8" x2="-198.12" y2="-185.42" width="0.1524" layer="91"/>
<wire x1="-198.12" y1="-185.42" x2="-180.34" y2="-185.42" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="GND@3"/>
<pinref part="U$2" gate="G$1" pin="GND@2"/>
<wire x1="-165.1" y1="-172.72" x2="-165.1" y2="-177.8" width="0.1524" layer="91"/>
<wire x1="-165.1" y1="-177.8" x2="-165.1" y2="-185.42" width="0.1524" layer="91"/>
<wire x1="-165.1" y1="-185.42" x2="-180.34" y2="-185.42" width="0.1524" layer="91"/>
<junction x="-165.1" y="-177.8"/>
<junction x="-180.34" y="-185.42"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="GND"/>
<wire x1="-198.12" y1="-142.24" x2="-220.98" y2="-142.24" width="0.1524" layer="91"/>
<pinref part="GND12" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="GROUND/ADJUST"/>
<wire x1="-172.72" y1="-246.38" x2="-182.88" y2="-246.38" width="0.1524" layer="91"/>
<pinref part="GND13" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="GND"/>
<wire x1="40.64" y1="-289.56" x2="48.26" y2="-289.56" width="0.1524" layer="91"/>
<pinref part="GND14" gate="1" pin="GND"/>
</segment>
</net>
<net name="FTDI_MASTER_RX" class="0">
<segment>
<wire x1="243.84" y1="-30.48" x2="231.14" y2="-30.48" width="0.1524" layer="91"/>
<label x="243.84" y="-30.48" size="1.778" layer="95" xref="yes"/>
<pinref part="U$9" gate="2X4" pin="P$8"/>
</segment>
<segment>
<pinref part="MASTER" gate="G$1" pin="PD1_[PCINT17]"/>
<wire x1="-58.42" y1="-124.46" x2="-58.42" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="-58.42" y1="-116.84" x2="-63.5" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="-63.5" y1="-116.84" x2="-63.5" y2="-111.76" width="0.1524" layer="91"/>
<wire x1="-63.5" y1="-111.76" x2="-66.04" y2="-111.76" width="0.1524" layer="91"/>
<label x="-66.04" y="-111.76" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="FTDI_SLAVE_RX" class="0">
<segment>
<wire x1="193.04" y1="-30.48" x2="205.74" y2="-30.48" width="0.1524" layer="91"/>
<label x="193.04" y="-30.48" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U$9" gate="2X4" pin="P$1"/>
</segment>
<segment>
<pinref part="SLAVE" gate="G$1" pin="PD1_[PCINT17]"/>
<wire x1="68.58" y1="-129.54" x2="68.58" y2="-121.92" width="0.1524" layer="91"/>
<wire x1="68.58" y1="-121.92" x2="63.5" y2="-121.92" width="0.1524" layer="91"/>
<wire x1="63.5" y1="-121.92" x2="63.5" y2="-119.38" width="0.1524" layer="91"/>
<label x="63.5" y="-119.38" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="FTDI_SLAVE_TX" class="0">
<segment>
<wire x1="205.74" y1="-35.56" x2="193.04" y2="-35.56" width="0.1524" layer="91"/>
<label x="193.04" y="-35.56" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U$9" gate="2X4" pin="P$2"/>
</segment>
<segment>
<pinref part="SLAVE" gate="G$1" pin="PD0_[PCINT16]"/>
<wire x1="71.12" y1="-129.54" x2="71.12" y2="-119.38" width="0.1524" layer="91"/>
<wire x1="71.12" y1="-119.38" x2="68.58" y2="-119.38" width="0.1524" layer="91"/>
<label x="68.58" y="-119.38" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="FTDI_SLAVE_DTR" class="0">
<segment>
<wire x1="205.74" y1="-40.64" x2="193.04" y2="-40.64" width="0.1524" layer="91"/>
<label x="193.04" y="-40.64" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U$9" gate="2X4" pin="P$3"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="83.82" y1="-99.06" x2="83.82" y2="-81.28" width="0.1524" layer="91"/>
<label x="83.82" y="-81.28" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="FTDI_MASTER_TX" class="0">
<segment>
<pinref part="MASTER" gate="G$1" pin="PD0_[PCINT16]"/>
<wire x1="-55.88" y1="-124.46" x2="-55.88" y2="-114.3" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="-114.3" x2="-60.96" y2="-114.3" width="0.1524" layer="91"/>
<wire x1="-60.96" y1="-114.3" x2="-60.96" y2="-111.76" width="0.1524" layer="91"/>
<label x="-60.96" y="-111.76" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<wire x1="231.14" y1="-35.56" x2="243.84" y2="-35.56" width="0.1524" layer="91"/>
<label x="243.84" y="-35.56" size="1.778" layer="95" xref="yes"/>
<pinref part="U$9" gate="2X4" pin="P$7"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="MASTER" gate="G$1" pin="PB6_[PCINT6]"/>
<wire x1="-76.2" y1="-175.26" x2="-91.44" y2="-175.26" width="0.1524" layer="91"/>
<wire x1="-91.44" y1="-175.26" x2="-91.44" y2="-167.64" width="0.1524" layer="91"/>
<wire x1="-91.44" y1="-167.64" x2="-99.06" y2="-167.64" width="0.1524" layer="91"/>
<pinref part="Y2" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="MASTER" gate="G$1" pin="PB7_[PCINT7]"/>
<wire x1="-99.06" y1="-182.88" x2="-91.44" y2="-182.88" width="0.1524" layer="91"/>
<wire x1="-91.44" y1="-182.88" x2="-91.44" y2="-177.8" width="0.1524" layer="91"/>
<wire x1="-91.44" y1="-177.8" x2="-76.2" y2="-177.8" width="0.1524" layer="91"/>
<pinref part="Y2" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="SLAVE" gate="G$1" pin="PB7_[PCINT7]"/>
<wire x1="50.8" y1="-182.88" x2="38.1" y2="-182.88" width="0.1524" layer="91"/>
<wire x1="38.1" y1="-182.88" x2="38.1" y2="-193.04" width="0.1524" layer="91"/>
<wire x1="38.1" y1="-193.04" x2="30.48" y2="-193.04" width="0.1524" layer="91"/>
<pinref part="Y1" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="SLAVE" gate="G$1" pin="PB6_[PCINT6]"/>
<wire x1="50.8" y1="-180.34" x2="38.1" y2="-180.34" width="0.1524" layer="91"/>
<wire x1="38.1" y1="-180.34" x2="38.1" y2="-177.8" width="0.1524" layer="91"/>
<wire x1="38.1" y1="-177.8" x2="30.48" y2="-177.8" width="0.1524" layer="91"/>
<pinref part="Y1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="MASTER" gate="G$1" pin="AVCC"/>
<wire x1="-25.4" y1="-175.26" x2="-53.34" y2="-175.26" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="-175.26" x2="-53.34" y2="-172.72" width="0.1524" layer="91"/>
<pinref part="P+1" gate="VCC" pin="VCC"/>
<wire x1="-83.82" y1="-167.64" x2="-83.82" y2="-170.18" width="0.1524" layer="91"/>
<pinref part="MASTER" gate="G$1" pin="VCC_2"/>
<wire x1="-76.2" y1="-172.72" x2="-81.28" y2="-172.72" width="0.1524" layer="91"/>
<pinref part="MASTER" gate="G$1" pin="VCC_1"/>
<wire x1="-81.28" y1="-172.72" x2="-81.28" y2="-170.18" width="0.1524" layer="91"/>
<wire x1="-81.28" y1="-170.18" x2="-81.28" y2="-167.64" width="0.1524" layer="91"/>
<wire x1="-81.28" y1="-167.64" x2="-76.2" y2="-167.64" width="0.1524" layer="91"/>
<wire x1="-83.82" y1="-170.18" x2="-81.28" y2="-170.18" width="0.1524" layer="91"/>
<junction x="-81.28" y="-170.18"/>
<wire x1="-53.34" y1="-172.72" x2="-76.2" y2="-172.72" width="0.1524" layer="91"/>
<junction x="-76.2" y="-172.72"/>
</segment>
<segment>
<pinref part="P+6" gate="VCC" pin="VCC"/>
<wire x1="38.1" y1="-172.72" x2="38.1" y2="-175.26" width="0.1524" layer="91"/>
<pinref part="SLAVE" gate="G$1" pin="VCC_2"/>
<wire x1="50.8" y1="-177.8" x2="48.26" y2="-177.8" width="0.1524" layer="91"/>
<wire x1="48.26" y1="-177.8" x2="48.26" y2="-175.26" width="0.1524" layer="91"/>
<pinref part="SLAVE" gate="G$1" pin="VCC_1"/>
<wire x1="48.26" y1="-175.26" x2="48.26" y2="-172.72" width="0.1524" layer="91"/>
<wire x1="48.26" y1="-172.72" x2="50.8" y2="-172.72" width="0.1524" layer="91"/>
<wire x1="38.1" y1="-175.26" x2="48.26" y2="-175.26" width="0.1524" layer="91"/>
<junction x="48.26" y="-175.26"/>
<pinref part="SLAVE" gate="G$1" pin="AVCC"/>
<wire x1="101.6" y1="-180.34" x2="73.66" y2="-180.34" width="0.1524" layer="91"/>
<wire x1="73.66" y1="-180.34" x2="73.66" y2="-177.8" width="0.1524" layer="91"/>
<wire x1="73.66" y1="-177.8" x2="50.8" y2="-177.8" width="0.1524" layer="91"/>
<junction x="50.8" y="-177.8"/>
</segment>
<segment>
<pinref part="P+5" gate="VCC" pin="VCC"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="-55.88" y1="-86.36" x2="-55.88" y2="-93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+3" gate="VCC" pin="VCC"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="73.66" y1="-83.82" x2="73.66" y2="-99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+7" gate="VCC" pin="VCC"/>
<wire x1="205.74" y1="-45.72" x2="198.12" y2="-45.72" width="0.1524" layer="91"/>
<pinref part="U$9" gate="2X4" pin="P$4"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="P$8"/>
<pinref part="P+2" gate="VCC" pin="VCC"/>
<wire x1="-50.8" y1="-22.86" x2="-43.18" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="-22.86" x2="-43.18" y2="-15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$7" gate="G$1" pin="P$8"/>
<pinref part="P+4" gate="VCC" pin="VCC"/>
<wire x1="73.66" y1="-25.4" x2="81.28" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="81.28" y1="-25.4" x2="81.28" y2="-22.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="VIN"/>
<wire x1="162.56" y1="-160.02" x2="139.7" y2="-160.02" width="0.1524" layer="91"/>
<pinref part="P+8" gate="VCC" pin="VCC"/>
<pinref part="U$5" gate="G$1" pin="CSB"/>
<wire x1="139.7" y1="-160.02" x2="139.7" y2="-157.48" width="0.1524" layer="91"/>
<wire x1="162.56" y1="-195.58" x2="139.7" y2="-195.58" width="0.1524" layer="91"/>
<wire x1="139.7" y1="-195.58" x2="139.7" y2="-160.02" width="0.1524" layer="91"/>
<junction x="139.7" y="-160.02"/>
</segment>
<segment>
<pinref part="P+9" gate="VCC" pin="VCC"/>
<pinref part="U$8" gate="G$1" pin="VCC"/>
<wire x1="53.34" y1="-109.22" x2="53.34" y2="-111.76" width="0.1524" layer="91"/>
<wire x1="53.34" y1="-111.76" x2="45.72" y2="-111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="5V"/>
<wire x1="154.94" y1="-226.06" x2="129.54" y2="-226.06" width="0.1524" layer="91"/>
<pinref part="P+10" gate="VCC" pin="VCC"/>
<wire x1="129.54" y1="-226.06" x2="129.54" y2="-223.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="VIN"/>
<wire x1="-172.72" y1="-256.54" x2="-182.88" y2="-256.54" width="0.1524" layer="91"/>
<pinref part="P+11" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="VCC"/>
<wire x1="40.64" y1="-294.64" x2="45.72" y2="-294.64" width="0.1524" layer="91"/>
<wire x1="45.72" y1="-294.64" x2="45.72" y2="-299.72" width="0.1524" layer="91"/>
<wire x1="45.72" y1="-299.72" x2="48.26" y2="-299.72" width="0.1524" layer="91"/>
<pinref part="P+12" gate="VCC" pin="VCC"/>
</segment>
</net>
<net name="FTDI_MASTER_DTR" class="0">
<segment>
<wire x1="231.14" y1="-40.64" x2="243.84" y2="-40.64" width="0.1524" layer="91"/>
<label x="243.84" y="-40.64" size="1.778" layer="95" xref="yes"/>
<pinref part="U$9" gate="2X4" pin="P$6"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="-45.72" y1="-99.06" x2="-45.72" y2="-88.9" width="0.1524" layer="91"/>
<label x="-45.72" y="-88.9" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="MISO_MASTER" class="0">
<segment>
<pinref part="U$6" gate="G$1" pin="P$1"/>
<wire x1="-76.2" y1="-22.86" x2="-78.74" y2="-22.86" width="0.1524" layer="91"/>
<label x="-78.74" y="-22.86" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="MASTER" gate="G$1" pin="PB4_[PCINT4]"/>
<wire x1="-43.18" y1="-205.74" x2="-43.18" y2="-208.28" width="0.1524" layer="91"/>
<label x="-35.56" y="-208.28" size="1.778" layer="95" rot="R270" xref="yes"/>
<wire x1="-43.18" y1="-208.28" x2="-35.56" y2="-208.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="MISO"/>
<wire x1="-198.12" y1="-162.56" x2="-205.74" y2="-162.56" width="0.1524" layer="91"/>
<label x="-205.74" y="-162.56" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SCK_MASTER" class="0">
<segment>
<pinref part="U$6" gate="G$1" pin="P$2"/>
<wire x1="-76.2" y1="-27.94" x2="-78.74" y2="-27.94" width="0.1524" layer="91"/>
<label x="-78.74" y="-27.94" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="MASTER" gate="G$1" pin="PB5_[PCINT5]"/>
<wire x1="-25.4" y1="-177.8" x2="-22.86" y2="-177.8" width="0.1524" layer="91"/>
<label x="-22.86" y="-177.8" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="SCK"/>
<wire x1="-198.12" y1="-157.48" x2="-205.74" y2="-157.48" width="0.1524" layer="91"/>
<label x="-205.74" y="-157.48" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="RESET_MASTER" class="0">
<segment>
<pinref part="U$6" gate="G$1" pin="P$3"/>
<wire x1="-76.2" y1="-33.02" x2="-78.74" y2="-33.02" width="0.1524" layer="91"/>
<label x="-78.74" y="-33.02" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="MASTER" gate="G$1" pin="PC6_[RESET/PCINT14]"/>
<wire x1="-53.34" y1="-124.46" x2="-53.34" y2="-111.76" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="-111.76" x2="-55.88" y2="-111.76" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="-53.34" y1="-111.76" x2="-50.8" y2="-111.76" width="0.1524" layer="91"/>
<junction x="-53.34" y="-111.76"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="-50.8" y1="-111.76" x2="-45.72" y2="-111.76" width="0.1524" layer="91"/>
<label x="-50.8" y="-78.74" size="1.778" layer="95" rot="R90" xref="yes"/>
<wire x1="-50.8" y1="-111.76" x2="-50.8" y2="-78.74" width="0.1524" layer="91"/>
<junction x="-50.8" y="-111.76"/>
</segment>
</net>
<net name="MOSI_MASTER" class="0">
<segment>
<pinref part="U$6" gate="G$1" pin="P$7"/>
<wire x1="-50.8" y1="-27.94" x2="-48.26" y2="-27.94" width="0.1524" layer="91"/>
<label x="-48.26" y="-27.94" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="MASTER" gate="G$1" pin="PB3_[PCINT3]"/>
<label x="-40.64" y="-210.82" size="1.778" layer="95" rot="R270" xref="yes"/>
<wire x1="-45.72" y1="-205.74" x2="-45.72" y2="-210.82" width="0.1524" layer="91"/>
<wire x1="-45.72" y1="-210.82" x2="-40.64" y2="-210.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="MOSI"/>
<wire x1="-198.12" y1="-167.64" x2="-205.74" y2="-167.64" width="0.1524" layer="91"/>
<label x="-205.74" y="-167.64" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="MISO_SLAVE" class="0">
<segment>
<pinref part="U$7" gate="G$1" pin="P$1"/>
<wire x1="48.26" y1="-25.4" x2="45.72" y2="-25.4" width="0.1524" layer="91"/>
<label x="45.72" y="-25.4" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SLAVE" gate="G$1" pin="PB4_[PCINT4]"/>
<wire x1="83.82" y1="-210.82" x2="83.82" y2="-213.36" width="0.1524" layer="91"/>
<wire x1="83.82" y1="-213.36" x2="91.44" y2="-213.36" width="0.1524" layer="91"/>
<label x="91.44" y="-213.36" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="MISO"/>
<wire x1="154.94" y1="-231.14" x2="147.32" y2="-231.14" width="0.1524" layer="91"/>
<label x="147.32" y="-231.14" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SCK_SLAVE" class="0">
<segment>
<pinref part="U$7" gate="G$1" pin="P$2"/>
<wire x1="48.26" y1="-30.48" x2="45.72" y2="-30.48" width="0.1524" layer="91"/>
<label x="45.72" y="-30.48" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SLAVE" gate="G$1" pin="PB5_[PCINT5]"/>
<wire x1="101.6" y1="-182.88" x2="104.14" y2="-182.88" width="0.1524" layer="91"/>
<label x="104.14" y="-182.88" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="SCK"/>
<wire x1="154.94" y1="-241.3" x2="147.32" y2="-241.3" width="0.1524" layer="91"/>
<label x="147.32" y="-241.3" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="RESET_SLAVE" class="0">
<segment>
<pinref part="U$7" gate="G$1" pin="P$3"/>
<wire x1="48.26" y1="-35.56" x2="45.72" y2="-35.56" width="0.1524" layer="91"/>
<label x="45.72" y="-35.56" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SLAVE" gate="G$1" pin="PC6_[RESET/PCINT14]"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="73.66" y1="-129.54" x2="73.66" y2="-116.84" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="73.66" y1="-116.84" x2="78.74" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-116.84" x2="83.82" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="83.82" y1="-116.84" x2="83.82" y2="-111.76" width="0.1524" layer="91"/>
<junction x="73.66" y="-116.84"/>
<label x="78.74" y="-81.28" size="1.778" layer="95" rot="R90" xref="yes"/>
<wire x1="78.74" y1="-81.28" x2="78.74" y2="-116.84" width="0.1524" layer="91"/>
<junction x="78.74" y="-116.84"/>
</segment>
</net>
<net name="MOSI_SLAVE" class="0">
<segment>
<pinref part="U$7" gate="G$1" pin="P$7"/>
<wire x1="73.66" y1="-30.48" x2="76.2" y2="-30.48" width="0.1524" layer="91"/>
<label x="76.2" y="-30.48" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="SLAVE" gate="G$1" pin="PB3_[PCINT3]"/>
<wire x1="81.28" y1="-210.82" x2="81.28" y2="-215.9" width="0.1524" layer="91"/>
<wire x1="81.28" y1="-215.9" x2="86.36" y2="-215.9" width="0.1524" layer="91"/>
<label x="86.36" y="-215.9" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="MOSI"/>
<wire x1="154.94" y1="-236.22" x2="147.32" y2="-236.22" width="0.1524" layer="91"/>
<label x="147.32" y="-236.22" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="SLAVE" gate="G$1" pin="PC5_[PCINT13]"/>
<wire x1="76.2" y1="-129.54" x2="76.2" y2="-119.38" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-119.38" x2="88.9" y2="-119.38" width="0.1524" layer="91"/>
<wire x1="88.9" y1="-119.38" x2="88.9" y2="-101.6" width="0.1524" layer="91"/>
<label x="88.9" y="-101.6" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="SCL"/>
<wire x1="162.56" y1="-175.26" x2="160.02" y2="-175.26" width="0.1524" layer="91"/>
<label x="160.02" y="-175.26" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="MASTER" gate="G$1" pin="PC5_[PCINT13]"/>
<wire x1="-50.8" y1="-124.46" x2="-50.8" y2="-114.3" width="0.1524" layer="91"/>
<wire x1="-50.8" y1="-114.3" x2="-40.64" y2="-114.3" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="-114.3" x2="-40.64" y2="-101.6" width="0.1524" layer="91"/>
<label x="-40.64" y="-101.6" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="SLAVE" gate="G$1" pin="PC4_[PCINT12]"/>
<wire x1="78.74" y1="-129.54" x2="78.74" y2="-121.92" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-121.92" x2="91.44" y2="-121.92" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-121.92" x2="91.44" y2="-109.22" width="0.1524" layer="91"/>
<label x="91.44" y="-109.22" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="SDA"/>
<wire x1="162.56" y1="-180.34" x2="160.02" y2="-180.34" width="0.1524" layer="91"/>
<label x="160.02" y="-180.34" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="MASTER" gate="G$1" pin="PC4_[PCINT12]"/>
<wire x1="-48.26" y1="-124.46" x2="-48.26" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="-48.26" y1="-116.84" x2="-35.56" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="-35.56" y1="-116.84" x2="-35.56" y2="-101.6" width="0.1524" layer="91"/>
<label x="-35.56" y="-101.6" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="DATA"/>
<pinref part="SLAVE" gate="G$1" pin="PD2_[PCINT18]"/>
<wire x1="40.64" y1="-111.76" x2="40.64" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="40.64" y1="-129.54" x2="66.04" y2="-129.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SS/CS_SLAVE" class="0">
<segment>
<pinref part="U$3" gate="G$1" pin="CS"/>
<wire x1="154.94" y1="-246.38" x2="147.32" y2="-246.38" width="0.1524" layer="91"/>
<label x="147.32" y="-246.38" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SLAVE" gate="G$1" pin="PB2_[PCINT2]"/>
<wire x1="78.74" y1="-210.82" x2="78.74" y2="-215.9" width="0.1524" layer="91"/>
<label x="78.74" y="-215.9" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="NSS_LORA" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="NSS"/>
<wire x1="-198.12" y1="-172.72" x2="-205.74" y2="-172.72" width="0.1524" layer="91"/>
<label x="-205.74" y="-172.72" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="MASTER" gate="G$1" pin="PB2_[PCINT2]"/>
<wire x1="-48.26" y1="-205.74" x2="-48.26" y2="-213.36" width="0.1524" layer="91"/>
<wire x1="-48.26" y1="-213.36" x2="-45.72" y2="-213.36" width="0.1524" layer="91"/>
<label x="-45.72" y="-213.36" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="DIO0_LORA" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="DIO0"/>
<wire x1="-165.1" y1="-157.48" x2="-154.94" y2="-157.48" width="0.1524" layer="91"/>
<label x="-154.94" y="-157.48" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="MASTER" gate="G$1" pin="PD2_[PCINT18]"/>
<wire x1="-60.96" y1="-124.46" x2="-60.96" y2="-119.38" width="0.1524" layer="91"/>
<wire x1="-60.96" y1="-119.38" x2="-68.58" y2="-119.38" width="0.1524" layer="91"/>
<label x="-68.58" y="-119.38" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="RST_LORA" class="0">
<segment>
<pinref part="MASTER" gate="G$1" pin="PD5_[PCINT21]"/>
<wire x1="-60.96" y1="-205.74" x2="-60.96" y2="-210.82" width="0.1524" layer="91"/>
<label x="-60.96" y="-210.82" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="RST"/>
<wire x1="-165.1" y1="-162.56" x2="-154.94" y2="-162.56" width="0.1524" layer="91"/>
<label x="-154.94" y="-162.56" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="VOUT"/>
<wire x1="-172.72" y1="-251.46" x2="-180.34" y2="-251.46" width="0.1524" layer="91"/>
<wire x1="-180.34" y1="-251.46" x2="-180.34" y2="-238.76" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="TAB_(OUTPUT)"/>
<wire x1="-180.34" y1="-238.76" x2="-157.48" y2="-238.76" width="0.1524" layer="91"/>
<wire x1="-157.48" y1="-231.14" x2="-157.48" y2="-238.76" width="0.1524" layer="91"/>
<wire x1="-157.48" y1="-238.76" x2="-132.08" y2="-238.76" width="0.1524" layer="91"/>
<wire x1="-132.08" y1="-238.76" x2="-132.08" y2="-251.46" width="0.1524" layer="91"/>
<pinref part="+3V1" gate="G$1" pin="+3V3"/>
<junction x="-157.48" y="-231.14"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="3.3V"/>
<wire x1="-165.1" y1="-167.64" x2="-134.62" y2="-167.64" width="0.1524" layer="91"/>
<pinref part="+3V2" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="+3V3" gate="G$1" pin="+3V3"/>
<pinref part="U$5" gate="G$1" pin="3.3V"/>
<wire x1="157.48" y1="-165.1" x2="162.56" y2="-165.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RX_GPS" class="0">
<segment>
<pinref part="U$4" gate="G$1" pin="RXD"/>
<wire x1="40.64" y1="-279.4" x2="48.26" y2="-279.4" width="0.1524" layer="91"/>
<label x="48.26" y="-279.4" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="68.58" y1="-236.22" x2="68.58" y2="-241.3" width="0.1524" layer="91"/>
<label x="68.58" y="-241.3" size="1.778" layer="95" rot="R270" xref="yes"/>
<pinref part="R3" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="2.54" y1="-162.56" x2="5.08" y2="-162.56" width="0.1524" layer="91"/>
<label x="5.08" y="-162.56" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="TX_GPS" class="0">
<segment>
<pinref part="U$4" gate="G$1" pin="TXD"/>
<wire x1="40.64" y1="-284.48" x2="48.26" y2="-284.48" width="0.1524" layer="91"/>
<label x="48.26" y="-284.48" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="73.66" y1="-236.22" x2="73.66" y2="-241.3" width="0.1524" layer="91"/>
<label x="73.66" y="-241.3" size="1.778" layer="95" rot="R270" xref="yes"/>
<pinref part="R4" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="2.54" y1="-154.94" x2="5.08" y2="-154.94" width="0.1524" layer="91"/>
<label x="5.08" y="-154.94" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="." gate="G$1" pin="O"/>
<pinref part="MASTER" gate="G$1" pin="PD3_[PCINT19]"/>
<wire x1="-81.28" y1="-160.02" x2="-76.2" y2="-160.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part=".@1" gate="G$1" pin="O"/>
<pinref part="MASTER" gate="G$1" pin="PD4_[PCINT20]"/>
<wire x1="-81.28" y1="-162.56" x2="-76.2" y2="-162.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="U$18" gate="G$1" pin="O"/>
<pinref part="MASTER" gate="G$1" pin="PD6_[PCINT22]"/>
<wire x1="-58.42" y1="-208.28" x2="-58.42" y2="-205.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="MASTER" gate="G$1" pin="PD7_[PCINT23]"/>
<pinref part="U$19" gate="G$1" pin="O"/>
<wire x1="-55.88" y1="-205.74" x2="-55.88" y2="-208.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="U$21" gate="G$1" pin="O"/>
<pinref part="MASTER" gate="G$1" pin="PB0_[PCINT0]"/>
<wire x1="-53.34" y1="-208.28" x2="-53.34" y2="-205.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="MASTER" gate="G$1" pin="PB1_[PCINT1]"/>
<pinref part="U$20" gate="G$1" pin="O"/>
<wire x1="-50.8" y1="-205.74" x2="-50.8" y2="-208.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="U$15" gate="G$1" pin="O"/>
<pinref part="MASTER" gate="G$1" pin="ADC7"/>
<wire x1="-20.32" y1="-165.1" x2="-25.4" y2="-165.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="U$16" gate="G$1" pin="O"/>
<pinref part="MASTER" gate="G$1" pin="AREF"/>
<wire x1="-20.32" y1="-170.18" x2="-25.4" y2="-170.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="U$17" gate="G$1" pin="O"/>
<pinref part="MASTER" gate="G$1" pin="ADC6"/>
<wire x1="-20.32" y1="-172.72" x2="-25.4" y2="-172.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="SLAVE" gate="G$1" pin="PD3_[PCINT19]"/>
<pinref part="U$22" gate="G$1" pin="O"/>
<wire x1="50.8" y1="-165.1" x2="45.72" y2="-165.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="SLAVE" gate="G$1" pin="PD4_[PCINT20]"/>
<pinref part="U$11" gate="G$1" pin="O"/>
<wire x1="50.8" y1="-167.64" x2="45.72" y2="-167.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="SLAVE" gate="G$1" pin="PC2_[PCINT10]"/>
<pinref part="U$25" gate="G$1" pin="O"/>
<wire x1="83.82" y1="-129.54" x2="83.82" y2="-127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="SLAVE" gate="G$1" pin="PC3_[PCINT11]"/>
<pinref part="U$24" gate="G$1" pin="O"/>
<wire x1="81.28" y1="-129.54" x2="81.28" y2="-127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="SLAVE" gate="G$1" pin="PC1_[PCINT9]"/>
<pinref part="U$26" gate="G$1" pin="O"/>
<wire x1="101.6" y1="-165.1" x2="106.68" y2="-165.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="SLAVE" gate="G$1" pin="PC0_[PCINT8]"/>
<pinref part="U$27" gate="G$1" pin="O"/>
<wire x1="101.6" y1="-167.64" x2="106.68" y2="-167.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="SLAVE" gate="G$1" pin="ADC7"/>
<pinref part="U$28" gate="G$1" pin="O"/>
<wire x1="101.6" y1="-170.18" x2="106.68" y2="-170.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="SLAVE" gate="G$1" pin="AREF"/>
<pinref part="U$29" gate="G$1" pin="O"/>
<wire x1="101.6" y1="-175.26" x2="106.68" y2="-175.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="SLAVE" gate="G$1" pin="ADC6"/>
<pinref part="U$30" gate="G$1" pin="O"/>
<wire x1="101.6" y1="-177.8" x2="106.68" y2="-177.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="SLAVE" gate="G$1" pin="PD6_[PCINT22]"/>
<pinref part="U$31" gate="G$1" pin="O"/>
<wire x1="68.58" y1="-210.82" x2="68.58" y2="-213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="SLAVE" gate="G$1" pin="PD5_[PCINT21]"/>
<pinref part="U$32" gate="G$1" pin="O"/>
<wire x1="66.04" y1="-210.82" x2="66.04" y2="-213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="SLAVE" gate="G$1" pin="PB1_[PCINT1]"/>
<pinref part="U$23" gate="G$1" pin="O"/>
<wire x1="76.2" y1="-210.82" x2="76.2" y2="-213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<wire x1="68.58" y1="-218.44" x2="68.58" y2="-215.9" width="0.1524" layer="91"/>
<wire x1="68.58" y1="-215.9" x2="71.12" y2="-215.9" width="0.1524" layer="91"/>
<pinref part="SLAVE" gate="G$1" pin="PD7_[PCINT23]"/>
<wire x1="71.12" y1="-215.9" x2="71.12" y2="-210.82" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="SLAVE" gate="G$1" pin="PB0_[PCINT0]"/>
<wire x1="73.66" y1="-210.82" x2="73.66" y2="-218.44" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="MASTER" gate="G$1" pin="PC1_[PCINT9]"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="-25.4" y1="-160.02" x2="-25.4" y2="-154.94" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="-154.94" x2="-15.24" y2="-154.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="MASTER" gate="G$1" pin="PC0_[PCINT8]"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="-25.4" y1="-162.56" x2="-15.24" y2="-162.56" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
